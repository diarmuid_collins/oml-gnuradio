	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifdef HAVE_CONFIG_H
	#include "config.h"
	#endif

	#include <gnuradio/io_signature.h>
	#include "c_waterfall_sink_impl.h"
	#include <gnuradio/prefs.h>
	#include <string.h>
	#include <stdio.h>
	#include <volk/volk.h>
	#include <iostream>
	#include <oml2/omlc.h>
	//#define OML_FROM_MAIN /* IMPORTANT: instruct APPNAME_oml.h to enable the generated code, rather than just declaring prototypes; do this in only one source file */
	#include <APPNAME_oml.h>

	namespace gr {
	namespace omlforge {

	c_waterfall_sink::sptr
	c_waterfall_sink::make()
	{
		return gnuradio::get_initial_sptr
				(new c_waterfall_sink_impl());
	}

	/*
	 * The private constructor
	 */
	c_waterfall_sink_impl::c_waterfall_sink_impl()
	: gr::sync_block("c_waterfall_sink",
			gr::io_signature::make(0, 1, sizeof(gr_complex)),
			gr::io_signature::make(0, 0, 0))
	{
		d_fftsize = 1024; // fftsize : size of the FFT to compute and display. If using the PDU message port to plot samples, the length of each PDU must be a multiple of the FFT size.
		d_fftavg = 1.0; //fft alpha average
		d_nconnections = 1; //nconnections : number of signals to be connected to the sink. The PDU message port is always available for a connection, and this value must be set to 0 if only the PDU message port is being used.
		//	d_index = 0; //??
		d_center_freq = 0;//fc : center frequency of signal (use for x-axis labels)
		d_bandwidth = 100000;  //bw : bandwidth of signal (used to set x-axis labels)
		d_nrows = 200;
		d_last_time = 0;
		set_update_time(0.1);

		// Perform fftshift operation;
		// this is usually desired when plotting
		d_shift = true;

		d_fft = new fft::fft_complex(d_fftsize, true);
		d_fbuf = (float*)volk_malloc(d_fftsize*sizeof(float),
				volk_get_alignment());
		memset(d_fbuf, 0, d_fftsize*sizeof(float));

		d_index = 0;
		// save the last "connection" for the PDU memory
		for(int i = 0; i < d_nconnections; i++) {
			d_residbufs.push_back((gr_complex*)volk_malloc(d_fftsize*sizeof(gr_complex),
					volk_get_alignment()));
			d_magbufs.push_back((double*)volk_malloc(d_fftsize*sizeof(double),
					volk_get_alignment()));
			memset(d_residbufs[i], 0, d_fftsize*sizeof(gr_complex));
			memset(d_magbufs[i], 0, d_fftsize*sizeof(double));
		}

		d_residbufs.push_back((gr_complex*)volk_malloc(d_fftsize*sizeof(gr_complex),
				volk_get_alignment()));
		d_pdu_magbuf = (double*)volk_malloc(d_fftsize*sizeof(double)*d_nrows,
				volk_get_alignment());
		d_magbufs.push_back(d_pdu_magbuf);
		memset(d_pdu_magbuf, 0, d_fftsize*sizeof(double)*d_nrows);
		memset(d_residbufs[d_nconnections], 0, d_fftsize*sizeof(gr_complex));



		initialize();

		// setup output message port to post frequency when display is
		// double-clicked
		message_port_register_out(pmt::mp("freq"));
		message_port_register_in(pmt::mp("freq"));
		set_msg_handler(pmt::mp("freq"),
				boost::bind(&c_waterfall_sink_impl::handle_set_freq, this, _1));

		// setup PDU handling input port
		message_port_register_in(pmt::mp("in"));
		set_msg_handler(pmt::mp("in"),
				boost::bind(&c_waterfall_sink_impl::handle_pdus, this, _1));

	}

	/*
	 * Our virtual destructor.
	 */
	c_waterfall_sink_impl::~c_waterfall_sink_impl()
	{
		for(int i = 0; i < (int)d_residbufs.size(); i++) {
			volk_free(d_residbufs[i]);
			volk_free(d_magbufs[i]);
		}
		delete d_fft;
		volk_free(d_fbuf);
	}

	void
	c_waterfall_sink_impl::initialize()
	{



		int numplots = (d_nconnections > 0) ? d_nconnections : 1;


		set_fft_size(d_fftsize);
		set_frequency_range(d_center_freq, d_bandwidth);



		// initialize update time to 10 times a second
		set_update_time(0.1);
	}

	void
	c_waterfall_sink_impl::set_fft_size(const int fftsize)
	{
		d_fftsize = fftsize;
	}

	void
	c_waterfall_sink_impl::set_frequency_range(const double centerfreq,
			const double bandwidth)
	{
		d_center_freq = centerfreq;
		d_bandwidth = bandwidth;

	}

	int
	c_waterfall_sink_impl::fft_size() const
	{
		return d_fftsize;
	}

	void
	c_waterfall_sink_impl::set_fft_average(const float fftavg)
	{
		d_fftavg = fftavg;
	}

	void
	c_waterfall_sink_impl::set_update_time(double t)
	{
		//convert update time to ticks
		gr::high_res_timer_type tps = gr::high_res_timer_tps();
		d_update_time = t * tps;
		// d_main_gui->setUpdateTime(t);
		d_last_time = 0;
	}

	float
	c_waterfall_sink_impl::fft_average() const
	{
		return d_fftavg;
	}


	void
	c_waterfall_sink_impl::fft(float *data_out, const gr_complex *data_in, int size)
	{
		//   if(d_window.size()) {
		//volk_32fc_32f_multiply_32fc(d_fft->get_inbuf(), data_in,
		//                                  &d_window.front(), size);
		//    }
		//   else {
		memcpy(d_fft->get_inbuf(), data_in, sizeof(gr_complex)*size);
		//    }

		d_fft->execute();     // compute the fft

		volk_32fc_s32f_x2_power_spectral_density_32f(data_out, d_fft->get_outbuf(),
				size, 1.0, size);

		// Perform shift operation
		unsigned int len = (unsigned int)(floor(size/2.0));
		float *tmp = (float*)malloc(sizeof(float)*len);
		memcpy(tmp, &data_out[0], sizeof(float)*len);
		memcpy(&data_out[0], &data_out[len], sizeof(float)*(size - len));
		memcpy(&data_out[size - len], tmp, sizeof(float)*len);
		free(tmp);
	}


	void
	c_waterfall_sink_impl::fftresize()
	{
		gr::thread::scoped_lock lock(d_setlock);

		int newfftsize = d_fftsize;
		//  d_fftavg = d_fftavg;

		if(newfftsize != d_fftsize) {

			// Resize residbuf and replace data
			for(int i = 0; i < d_nconnections; i++) {
				volk_free(d_residbufs[i]);
				volk_free(d_magbufs[i]);

				d_residbufs[i] = (gr_complex*)volk_malloc(newfftsize*sizeof(gr_complex),
						volk_get_alignment());
				d_magbufs[i] = (double*)volk_malloc(newfftsize*sizeof(double),
						volk_get_alignment());

				memset(d_residbufs[i], 0, newfftsize*sizeof(gr_complex));
				memset(d_magbufs[i], 0, newfftsize*sizeof(double));
			}

			// Handle the PDU buffers separately because of the different
			// size requirement of the pdu_magbuf.
			volk_free(d_residbufs[d_nconnections]);
			volk_free(d_pdu_magbuf);

			d_residbufs[d_nconnections] = (gr_complex*)volk_malloc(newfftsize*sizeof(gr_complex),
					volk_get_alignment());
			d_pdu_magbuf = (double*)volk_malloc(newfftsize*sizeof(double)*d_nrows,
					volk_get_alignment());
			d_magbufs[d_nconnections] = d_pdu_magbuf;
			memset(d_residbufs[d_nconnections], 0, newfftsize*sizeof(gr_complex));
			memset(d_pdu_magbuf, 0, newfftsize*sizeof(double)*d_nrows);

			// Set new fft size and reset buffer index
			// (throws away any currently held data, but who cares?)
			d_fftsize = newfftsize;
			d_index = 0;

			// Reset window to reflect new size
			//  buildwindow();

			// Reset FFTW plan for new size
			delete d_fft;
			d_fft = new fft::fft_complex(d_fftsize, true);

			volk_free(d_fbuf);
			d_fbuf = (float*)volk_malloc(d_fftsize*sizeof(float),
					volk_get_alignment());
			memset(d_fbuf, 0, d_fftsize*sizeof(float));

			d_last_time = 0;
		}
	}

	void
	c_waterfall_sink_impl::handle_set_freq(pmt::pmt_t msg)
	{
		if(pmt::is_pair(msg)) {
			pmt::pmt_t x = pmt::cdr(msg);
			if(pmt::is_real(x)) {
				d_center_freq = pmt::to_double(x);
				//          d_qApplication->postEvent(d_main_gui,
				//                                    new SetFreqEvent(d_center_freq, d_bandwidth));
			}
		}
	}

	int
	c_waterfall_sink_impl::work(int noutput_items,
			gr_vector_const_void_star &input_items,
			gr_vector_void_star &output_items)
	{
		int j=0;
		const gr_complex *in = (const gr_complex*)input_items[0];

		// Update the FFT size from the application
		fftresize();
		//      windowreset();
		//      check_clicked();

		printf("Process packet data sent \n");

		for(int i=0; i < noutput_items; i+=d_fftsize) {
			unsigned int datasize = noutput_items - i;
			unsigned int resid = d_fftsize-d_index;

			// If we have enough input for one full FFT, do it
			if(datasize >= resid) {

				if(gr::high_res_timer_now() - d_last_time > d_update_time) {
					for(int n = 0; n < d_nconnections; n++) {
						// Fill up residbuf with d_fftsize number of items
						in = (const gr_complex*)input_items[n];
						memcpy(d_residbufs[n]+d_index, &in[j], sizeof(gr_complex)*resid);

						fft(d_fbuf, d_residbufs[n], d_fftsize);
						for(int x = 0; x < d_fftsize; x++) {
							d_magbufs[n][x] = (double)((1.0-d_fftavg)*d_magbufs[n][x] + (d_fftavg)*d_fbuf[x]);
							int result = oml_inject_waterfallplotmp(
									g_oml_mps_APPNAME->waterfallplotmp, (double)((1.0-d_fftavg)*d_magbufs[n][x] + (d_fftavg)*d_fbuf[x]),
									gr::high_res_timer_now(), 11);
							if (result == -1) {
								fprintf (stderr, "Could not send waterfall data to OML\n");
							}
							//printf("[d_start_freq] = %fl \n", d_start_freq);
							//							printf("[oml] =  %lf \n", (double)((1.0-d_fftavg)*d_magbufs[n][x] + (d_fftavg)*d_fbuf[x]));
							//							printf("[x] =  %u \n", x);
							//							printf("gr::high_res_timer_now() %llu", gr::high_res_timer_now());


						}
						//volk_32f_convert_64f(d_magbufs[n], d_fbuf, d_fftsize);
					}

					//		    d_qApplication->postEvent(d_main_gui,
					//					      new WaterfallUpdateEvent(d_magbufs,
					//								       d_fftsize,
					//								       d_last_time));
				}

				d_index = 0;
				j += resid;
			}
			// Otherwise, copy what we received into the residbuf for next time
			else {
//				printf("Load distinguishing data sent \n");
//				d_last_time = gr::high_res_timer_now();
//				//insert some junk data into OML database so we can differentiate between new and old packet data in waterfall graph.
//				for(int x = 0; x < 4000; x++) {
//					int result = oml_inject_waterfallplotmp(
//							g_oml_mps_APPNAME->waterfallplotmp, 0,
//							gr::high_res_timer_now(), 11);
//					if (result == -1) {
//						fprintf (stderr, "Could not send waterfall data to OML\n");
//					}
//					x++;
//				}

				for(int n = 0; n < d_nconnections; n++) {
					in = (const gr_complex*)input_items[n];
					memcpy(d_residbufs[n]+d_index, &in[j], sizeof(gr_complex)*datasize);
				}
				d_index += datasize;
				j += datasize;
			}
		}

		return j;
	}

	void
	c_waterfall_sink_impl::handle_pdus(pmt::pmt_t msg)
	{
		size_t len;
		size_t start = 0;
		pmt::pmt_t dict, samples;

		// Test to make sure this is either a PDU or a uniform vector of
		// samples. Get the samples PMT and the dictionary if it's a PDU.
		// If not, we throw an error and exit.
		if(pmt::is_pair(msg)) {
			dict = pmt::car(msg);
			samples = pmt::cdr(msg);

			pmt::pmt_t start_key = pmt::string_to_symbol("start");
			if(pmt::dict_has_key(dict, start_key)) {
				start = pmt::to_uint64(pmt::dict_ref(dict, start_key, pmt::PMT_NIL));
			}
		}
		else if(pmt::is_uniform_vector(msg)) {
			samples = msg;
		}
		else {
			throw std::runtime_error("time_sink_c: message must be either "
					"a PDU or a uniform vector of samples.");
		}

		len = pmt::length(samples);

		const gr_complex *in;
		if(pmt::is_c32vector(samples)) {
			in = (const gr_complex*)pmt::c32vector_elements(samples, len);
		}
		else {
			throw std::runtime_error("waterfall_sink_c: unknown data type "
					"of samples; must be complex.");
		}

		// Plot if we're past the last update time
		if(gr::high_res_timer_now() - d_last_time > d_update_time) {
			d_last_time = gr::high_res_timer_now();

			// Update the FFT size from the application
			fftresize();
			//       windowreset();
			//       check_clicked();

			gr::high_res_timer_type ref_start = (uint64_t)start * (double)(1.0/d_bandwidth) * 1000000;

			int stride = std::max(0, (int)(len - d_fftsize)/(int)(d_nrows));

			// set_time_per_fft(1.0/d_bandwidth * stride);
			std::ostringstream title("");
			title << "Time (+" << (uint64_t)ref_start << "us)";
			//  set_time_title(title.str());

			int j = 0;
			size_t min = 0;
			size_t max = std::min(d_fftsize, static_cast<int>(len));
			for(size_t i=0; j < d_nrows; i+=stride) {
				// Clear residbufs if len < d_fftsize
				memset(d_residbufs[d_nconnections], 0x00, sizeof(gr_complex)*d_fftsize);

				// Copy in as much of the input samples as we can
				memcpy(d_residbufs[d_nconnections], &in[min], sizeof(gr_complex)*(max-min));

				// Apply the window and FFT; copy data into the PDU
				// magnitude buffer.
				fft(d_fbuf, d_residbufs[d_nconnections], d_fftsize);
				for(int x = 0; x < d_fftsize; x++) {
					d_pdu_magbuf[j * d_fftsize + x] = (double)d_fbuf[x];
				}

				// Increment our indices; set max up to the number of
				// samples in the input PDU.
				min += stride;
				max = std::min(max + stride, len);
				j++;
			}

			//update gui per-pdu
			//       d_qApplication->postEvent(d_main_gui,
			//                                 new WaterfallUpdateEvent(d_magbufs,
			//                                                          d_fftsize*d_nrows,
			//                                                          0));
		}
	}

	} /* namespace omlforge */
	} /* namespace gr */

