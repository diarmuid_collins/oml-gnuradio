	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifdef HAVE_CONFIG_H
	#include "config.h"
	#endif

	#include <gnuradio/io_signature.h>
	#include "c_freq_sink_impl.h"
	#include <gnuradio/prefs.h>
	#include <string.h>
	#include <volk/volk.h>
	#include <gnuradio/basic_block.h>
	#include <gnuradio/fft/fft.h>
	#include <stdio.h>
	#include <cmath>
	#include <oml2/omlc.h>
	//#define OML_FROM_MAIN /* IMPORTANT: instruct APPNAME_oml.h to enable the generated code, rather than just declaring prototypes; do this in only one source file */
	#include <APPNAME_oml.h>

	namespace gr {
	namespace omlforge {

	c_freq_sink::sptr
	c_freq_sink::make()
	{
		return gnuradio::get_initial_sptr
				(new c_freq_sink_impl());
	}

	/*
	 * The private constructor
	 */
	c_freq_sink_impl::c_freq_sink_impl()
	: gr::sync_block("c_freq_sink",
			gr::io_signature::make(0, 1, sizeof(gr_complex)), // input signature
			gr::io_signature::make(0, 0, 0))
	{
		d_fftsize = 1024; // fftsize : size of the FFT to compute and display. If using the PDU message port to plot samples, the length of each PDU must be a multiple of the FFT size.
		d_fftavg = 1.0; //fft alpha average
		d_nconnections = 1; //nconnections : number of signals to be connected to the sink. The PDU message port is always available for a connection, and this value must be set to 0 if only the PDU message port is being used.
		d_index = 0; //??
		d_center_freq = 0;
		d_bandwidth = 100000;

		// setup output message port to post frequency when display is
		// double-clicked
		message_port_register_out(pmt::mp("freq"));
		message_port_register_in(pmt::mp("freq"));
		set_msg_handler(pmt::mp("freq"),
				boost::bind(&c_freq_sink_impl::handle_set_freq, this, _1));

		// setup PDU handling input port
		message_port_register_in(pmt::mp("in"));
		set_msg_handler(pmt::mp("in"),
				boost::bind(&c_freq_sink_impl::handle_pdus, this, _1));


		// Perform fftshift operation;
		// this is usually desired when plotting
		d_shift = true;
		//
		d_fft = new fft::fft_complex(d_fftsize, true);
		d_fbuf = (float*)volk_malloc(d_fftsize*sizeof(float),
				volk_get_alignment());
		memset(d_fbuf, 0, d_fftsize*sizeof(float));

		d_tmpbuflen = (unsigned int)(floor(d_fftsize/2.0));
		d_tmpbuf = (float*)volk_malloc(sizeof(float)*(d_tmpbuflen + 1),
				volk_get_alignment());


		d_index = 0;
		// save the last "connection" for the PDU memory
		for(int i = 0; i < d_nconnections; i++) {
			d_residbufs.push_back((gr_complex*)volk_malloc(d_fftsize*sizeof(gr_complex),
					volk_get_alignment()));
			d_magbufs.push_back((double*)volk_malloc(d_fftsize*sizeof(double),
					volk_get_alignment()));

			memset(d_residbufs[i], 0, d_fftsize*sizeof(gr_complex));
			memset(d_magbufs[i], 0, d_fftsize*sizeof(double));
		}

		d_residbufs.push_back((gr_complex*)volk_malloc(d_fftsize*sizeof(gr_complex),
				volk_get_alignment()));
		d_pdu_magbuf = (double*)volk_malloc(d_fftsize*sizeof(double),
				volk_get_alignment());
		d_magbufs.push_back(d_pdu_magbuf);
		memset(d_residbufs[d_nconnections], 0, d_fftsize*sizeof(gr_complex));
		memset(d_pdu_magbuf, 0, d_fftsize*sizeof(double));

		//buildwindow();

		initialize();

		set_trigger_mode(TRIG_MODE_FREE, 0, 0);
	}

	/*
	 * Our virtual destructor.
	 */

	c_freq_sink_impl::~c_freq_sink_impl()
	{
	}

	void
	c_freq_sink_impl::handle_set_freq(pmt::pmt_t msg)
	{
		if(pmt::is_pair(msg)) {
			pmt::pmt_t x = pmt::cdr(msg);
			if(pmt::is_real(x)) {
				d_center_freq = pmt::to_double(x);
				//          d_qApplication->postEvent(d_main_gui,
				//                                    new SetFreqEvent(d_center_freq, d_bandwidth));
			}
		}
	}

	void
	c_freq_sink_impl::initialize()
	{
		int numplots = (d_nconnections > 0) ? d_nconnections : 1;
		//   d_main_gui = new FreqDisplayForm(numplots, d_parent);


		//	      set_frequency_range(d_center_freq, d_bandwidth);

		//	      if(d_name.size() > 0)
		//	        set_title(d_name);

		set_output_multiple(d_fftsize);

		// initialize update time to 10 times a second
		set_update_time(0.1);
	}

	void
	c_freq_sink_impl::set_trigger_mode(trigger_mode mode,
			float level,
			int channel,
			const std::string &tag_key)
	{
		gr::thread::scoped_lock lock(d_setlock);

		d_trigger_count = 0;

		//	          d_main_gui->setTriggerMode(d_trigger_mode);
		//	          d_main_gui->setTriggerLevel(d_trigger_level);
		//	          d_main_gui->setTriggerChannel(d_trigger_channel);
		//	          d_main_gui->setTriggerTagKey(tag_key);

		_reset();
	}

	void
	c_freq_sink_impl::_reset()
	{
		d_trigger_count = 0;

		d_last_time = 0;
		// Reset the trigger.
		if(d_trigger_mode == TRIG_MODE_FREE) {
			d_triggered = true;
		}
		else {
			d_triggered = false;
		}
	}



	void
	c_freq_sink_impl::_test_trigger_norm(int nitems, std::vector<double*> inputs)
	{
		const double *in = (const double*)inputs[d_trigger_channel];
		for(int i = 0; i < nitems; i++) {
			d_trigger_count++;

			// Test if trigger has occurred based on the FFT magnitude and
			// channel number. Test if any value is > the level (in dBx).
			if(in[i] > d_trigger_level) {
				d_triggered = true;
				d_trigger_count = 0;
				break;
			}
		}

		// If using auto trigger mode, trigger periodically even
		// without a trigger event.
		if((d_trigger_mode == TRIG_MODE_AUTO) && (d_trigger_count > d_fftsize)) {
			d_triggered = true;
			d_trigger_count = 0;
		}
	}


	void
	c_freq_sink_impl::_test_trigger_tags(int start, int nitems)
	{
		uint64_t nr = nitems_read(d_trigger_channel);
		std::vector<gr::tag_t> tags;
		get_tags_in_range(tags, d_trigger_channel,
				nr+start, nr+start+nitems,
				d_trigger_tag_key);
		if(tags.size() > 0) {
			d_triggered = true;
			d_index = tags[0].offset - nr;
			d_trigger_count = 0;
		}
	}

	void
	c_freq_sink_impl::handle_pdus(pmt::pmt_t msg)
	{
		size_t len;
		pmt::pmt_t dict, samples;

		// Test to make sure this is either a PDU or a uniform vector of
		// samples. Get the samples PMT and the dictionary if it's a PDU.
		// If not, we throw an error and exit.
		if(pmt::is_pair(msg)) {
			dict = pmt::car(msg);
			samples = pmt::cdr(msg);
		}
		else if(pmt::is_uniform_vector(msg)) {
			samples = msg;
		}
		else {
			throw std::runtime_error("time_sink_c: message must be either "
					"a PDU or a uniform vector of samples.");
		}

		len = pmt::length(samples);

		const gr_complex *in;
		if(pmt::is_c32vector(samples)) {
			in = (const gr_complex*)pmt::c32vector_elements(samples, len);
		}
		else {
			throw std::runtime_error("freq_sink_c: unknown data type "
					"of samples; must be complex.");
		}

		// Plot if we're past the last update time
		if(gr::high_res_timer_now() - d_last_time > d_update_time) {
			d_last_time = gr::high_res_timer_now();

			// Update the FFT size from the application
			fftresize();
			//         windowreset();
			//         check_clicked();

			int winoverlap = 4;
			int fftoverlap = d_fftsize / winoverlap;
			float num = static_cast<float>(winoverlap * len) / static_cast<float>(d_fftsize);
			int nffts = static_cast<int>(ceilf(num));

			// Clear this as we will be accumulating in the for loop over nffts
			memset(d_pdu_magbuf, 0, sizeof(double)*d_fftsize);

			size_t min = 0;
			size_t max = std::min(d_fftsize, static_cast<int>(len));
			for(int n = 0; n < nffts; n++) {
				// Clear in case (max-min) < d_fftsize
				memset(d_residbufs[d_nconnections], 0x00, sizeof(gr_complex)*d_fftsize);

				// Copy in as much of the input samples as we can
				memcpy(d_residbufs[d_nconnections], &in[min], sizeof(gr_complex)*(max-min));

				// Apply the window and FFT; copy data into the PDU
				// magnitude buffer.
				fft(d_fbuf, d_residbufs[d_nconnections], d_fftsize);
				for(int x = 0; x < d_fftsize; x++) {
					d_pdu_magbuf[x] += (double)d_fbuf[x];
				}

				// Increment our indices; set max up to the number of
				// samples in the input PDU.
				min += fftoverlap;
				max = std::min(max + fftoverlap, len);
			}

			// Perform the averaging
			for(int x = 0; x < d_fftsize; x++) {
				d_pdu_magbuf[x] /= static_cast<double>(nffts);
			}

			//update gui per-pdu
			//         d_qApplication->postEvent(d_main_gui,
			//                                   new FreqUpdateEvent(d_magbufs, d_fftsize));
		}
	}



	void
	c_freq_sink_impl::fft(float *data_out, const gr_complex *data_in, int size)
	{
		//        if(d_window.size()) {
		//  	volk_32fc_32f_multiply_32fc(d_fft->get_inbuf(), data_in,
		//                                      &d_window.front(), size);
		//        }
		//        else {
		memcpy(d_fft->get_inbuf(), data_in, sizeof(gr_complex)*size);
		//        }

		d_fft->execute();     // compute the fft

		volk_32fc_s32f_x2_power_spectral_density_32f(data_out, d_fft->get_outbuf(),
				size, 1.0, size);

		// Perform shift operation
		memcpy(d_tmpbuf, &data_out[0], sizeof(float)*(d_tmpbuflen + 1));
		memcpy(&data_out[0], &data_out[size - d_tmpbuflen], sizeof(float)*(d_tmpbuflen));
		memcpy(&data_out[d_tmpbuflen], d_tmpbuf, sizeof(float)*(d_tmpbuflen + 1));
	}
	//
	//

	void
	c_freq_sink_impl::set_update_time(double t)
	{
		//convert update time to ticks
		gr::high_res_timer_type tps = gr::high_res_timer_tps();
		d_update_time = t * tps;
		// d_main_gui->setUpdateTime(t);
		d_last_time = 0;
	}

	bool
	c_freq_sink_impl::fftresize()
	{
		gr::thread::scoped_lock lock(d_setlock);

		//   int newfftsize = d_main_gui->getFFTSize();
		// d_fftavg = d_main_gui->getFFTAverage();

		if(d_fftsize != d_fftsize) {
			// Resize residbuf and replace data
			// +1 to handle PDU buffers
			for(int i = 0; i < d_nconnections+1; i++) {
				volk_free(d_residbufs[i]);
				volk_free(d_magbufs[i]);

				d_residbufs[i] = (gr_complex*)volk_malloc(d_fftsize*sizeof(gr_complex),
						volk_get_alignment());
				d_magbufs[i] = (double*)volk_malloc(d_fftsize*sizeof(double),
						volk_get_alignment());

				memset(d_residbufs[i], 0, d_fftsize*sizeof(gr_complex));
				memset(d_magbufs[i], 0, d_fftsize*sizeof(double));
			}

			// Update the pointer to the newly allocated memory
			d_pdu_magbuf = d_magbufs[d_nconnections];

			// Set new fft size and reset buffer index
			// (throws away any currently held data, but who cares?)
			//d_fftsize = d_fftsize;
			d_index = 0;

			// Reset window to reflect new size
			//	buildwindow();

			// Reset FFTW plan for new size
			delete d_fft;
			d_fft = new fft::fft_complex(d_fftsize, true);

			volk_free(d_fbuf);
			d_fbuf = (float*)volk_malloc(d_fftsize*sizeof(float),
					volk_get_alignment());
			memset(d_fbuf, 0, d_fftsize*sizeof(float));

			volk_free(d_tmpbuf);
			d_tmpbuflen = (unsigned int)(floor(d_fftsize/2.0));
			d_tmpbuf = (float*)volk_malloc(sizeof(float)*(d_tmpbuflen + 1),
					volk_get_alignment());

			d_last_time = 0;

			set_output_multiple(d_fftsize);

			return true;
		}
		return false;
	}

	/*
	 * Our virtual destructor.
	 */

	int
	c_freq_sink_impl::work(int noutput_items,
			gr_vector_const_void_star &input_items,
			gr_vector_void_star &output_items){

		const gr_complex *in = (const gr_complex*)input_items[0];

		// Update the FFT size from the application
		bool updated = false;
		updated |= fftresize();
		//  updated |= windowreset();
		//  if(updated)
		//    return 0;

		//	      check_clicked();
		//	      _gui_update_trigger();

		gr::thread::scoped_lock lock(d_setlock);
		for(d_index = 0; d_index < noutput_items; d_index+=d_fftsize) {

			if((gr::high_res_timer_now() - d_last_time) > d_update_time) {

				// Trigger off tag, if active
				if((d_trigger_mode == TRIG_MODE_TAG) && !d_triggered) {
					_test_trigger_tags(d_index, d_fftsize);
					if(d_triggered) {
						// If not enough from tag position, early exit
						if((d_index + d_fftsize) >= noutput_items)
							return d_index;
					}
				}

				// Perform FFT and shift operations into d_magbufs
				for(int n = 0; n < d_nconnections; n++) {
					in = (const gr_complex*)input_items[n];
					memcpy(d_residbufs[n], &in[d_index], sizeof(gr_complex)*d_fftsize);

					fft(d_fbuf, d_residbufs[n], d_fftsize);
					for(int x = 0; x < d_fftsize; x++) {
						d_magbufs[n][x] = (double)((1.0-d_fftavg)*d_magbufs[n][x] + (d_fftavg)*d_fbuf[x]);
						//double baz = (double)((1.0-d_fftavg)*d_magbufs[n][x] + (d_fftavg)*d_fbuf[x]);
						//printf("%u , %f \n" , x, baz);
						int result = oml_inject_frequencyplotmp(
								g_oml_mps_APPNAME->frequencyplotmp, x,
								(double)((1.0-d_fftavg)*d_magbufs[n][x] + (d_fftavg)*d_fbuf[x]));
						if (result == -1) {
							fprintf (stderr, "Could not send to OML\n");
						}

					}
					//volk_32f_convert_64f(d_magbufs[n], d_fbuf, d_fftsize);
				}

				// Test trigger off signal power in d_magbufs
				if((d_trigger_mode == TRIG_MODE_NORM) || (d_trigger_mode == TRIG_MODE_AUTO)) {
					_test_trigger_norm(d_fftsize, d_magbufs);
				}

				// If a trigger (FREE always triggers), plot and reset state
				if(d_triggered) {
					d_last_time = gr::high_res_timer_now();
					//	            d_qApplication->postEvent(d_main_gui,
					//	                                      new FreqUpdateEvent(d_magbufs, d_fftsize));
					_reset();
				}
			}
		}

		return noutput_items;
	}


	} /* namespace omlforge */
	} /* namespace gr */

