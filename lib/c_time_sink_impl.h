	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifndef INCLUDED_OMLFORGE_C_TIME_SINK_IMPL_H
	#define INCLUDED_OMLFORGE_C_TIME_SINK_IMPL_H

	#include <omlforge/c_time_sink.h>
	#include <gnuradio/high_res_timer.h>
	#include <omlforge/trigger_mode.h>

	namespace gr {
	namespace omlforge {

	class c_time_sink_impl : public c_time_sink
	{
	private:
		void initialize();

		int d_size, d_buffer_size;
		double d_samp_rate;
		std::string d_name;
		int d_nconnections;

		gr::high_res_timer_type d_update_time;
		gr::high_res_timer_type d_last_time;

		std::vector<double*> d_residbufs_real;
		std::vector<double*> d_residbufs_imag;

		int d_index, d_start, d_end;
		std::vector<gr_complex*> d_cbuffers;
		std::vector<double*> d_buffers;
		std::vector< std::vector<gr::tag_t> > d_tags;
		void _reset();

		void _npoints_resize();

		void _gui_update_trigger();
		float d_trigger_level;
		int d_trigger_channel;
		int d_trigger_delay;
		pmt::pmt_t d_trigger_tag_key;
		bool d_triggered;
		int d_trigger_count;

		// Handles message input port for displaying PDU samples.
		void handle_pdus(pmt::pmt_t msg);


	public:
		c_time_sink_impl();
		~c_time_sink_impl();

		void set_nsamps(const int size);
		void set_samp_rate(const double samp_rate);
		int nsamps() const;
		void set_update_time(double t);
		// Where all the action really happens

		void _adjust_tags(int adj);

		// Members used for triggering scope
		trigger_mode d_trigger_mode;
		trigger_slope d_trigger_slope;

		void set_trigger_mode(trigger_mode mode, trigger_slope slope,
				float level, float delay, int channel,
				const std::string &tag_key="");

		void _test_trigger_tags(int nitems);
		void _test_trigger_norm(int nitems, gr_vector_const_void_star inputs);
		bool _test_trigger_slope(const gr_complex *in) const;
		// Where all the action really happens
		int work(int noutput_items,
				gr_vector_const_void_star &input_items,
				gr_vector_void_star &output_items);
	};

	} // namespace omlforge
	} // namespace gr

	#endif /* INCLUDED_OMLFORGE_C_TIME_SINK_IMPL_H */

