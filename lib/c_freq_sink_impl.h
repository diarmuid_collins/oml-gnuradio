	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifndef INCLUDED_OMLFORGE_C_FREQ_SINK_IMPL_H
	#define INCLUDED_OMLFORGE_C_FREQ_SINK_IMPL_H

	#include <omlforge/c_freq_sink.h>
	#include <gnuradio/high_res_timer.h>
	#include <omlforge/trigger_mode.h>
	#include <gnuradio/fft/fft.h>
	#include <gnuradio/fft/api.h>
	#include <gnuradio/gr_complex.h>

	namespace gr {
	namespace omlforge {

	class c_freq_sink_impl : public c_freq_sink
	{
	private:
		void initialize();

		int d_fftsize;
		int d_tmpbuflen;
		float d_fftavg;
		double d_center_freq;
		double d_bandwidth;
		int d_nconnections;

		bool d_shift;
		fft::fft_complex *d_fft;
		int d_index;
		std::vector<gr_complex*> d_residbufs;
		std::vector<double*> d_magbufs;
		double* d_pdu_magbuf;
		float *d_fbuf;
		float *d_tmpbuf;
		int d_trigger_channel;
		float d_trigger_level;

		pmt::pmt_t d_trigger_tag_key;
		gr::high_res_timer_type d_update_time;
		gr::high_res_timer_type d_last_time;
		void handle_set_freq(pmt::pmt_t msg);
		void handle_pdus(pmt::pmt_t msg);
		trigger_mode d_trigger_mode;

		void _test_trigger_tags(int start, int nitems);
		void _test_trigger_norm(int nitems, std::vector<double*> inputs);
		void set_update_time(double t);
		bool d_triggered;
		int d_trigger_count;
		void _reset();
		void fft(float *data_out, const gr_complex *data_in, int size);
		bool fftresize();

	public:
		c_freq_sink_impl();
		~c_freq_sink_impl();

		void set_trigger_mode(trigger_mode mode,
		float level, int channel,
				const std::string &tag_key="");

		void reset();
		// Where all the action really happens
		int work(int noutput_items,
				gr_vector_const_void_star &input_items,
				gr_vector_void_star &output_items);
	};

	} // namespace omlforge
	} // namespace gr

	#endif /* INCLUDED_OMLFORGE_C_FREQ_SINK_IMPL_H */

