	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifdef HAVE_CONFIG_H
	#include "config.h"
	#endif

	#include <gnuradio/io_signature.h>
	#include "c_time_sink_impl.h"
	#include <gnuradio/prefs.h>
	#include <string.h>
	#include <volk/volk.h>
	#include <gnuradio/fft/fft.h>
	#include <gnuradio/basic_block.h>
	#include <stdio.h>
	#include <cmath>
	#include <oml2/omlc.h>
	//#define OML_FROM_MAIN /* IMPORTANT: instruct APPNAME_oml.h to enable the generated code, rather than just declaring prototypes; do this in only one source file */
	#include <APPNAME_oml.h>

	namespace gr {
	namespace omlforge {

	c_time_sink::sptr
	c_time_sink::make()
	{
		return gnuradio::get_initial_sptr
				(new c_time_sink_impl());
	}

	/*
	 * The private constructor
	 */
	c_time_sink_impl::c_time_sink_impl()
	: gr::sync_block("c_time_sink",
			gr::io_signature::make(0, 1, sizeof(gr_complex)), // input signature
			gr::io_signature::make(0, 0, 0)) // output signature
	{
		d_size = 1024; // fftsize : size of the FFT to compute and display. If using the PDU message port to plot samples, the length of each PDU must be a multiple of the FFT size.
		d_nconnections = 2; //nconnections : number of signals to be connected to the sink. The PDU message port is always available for a connection, and this value must be set to 0 if only the PDU message port is being used.
		d_buffer_size = 2*d_size;
		d_end = d_size;
		d_samp_rate = 100000;
		d_index = 0;

		// setup PDU handling input port
		message_port_register_in(pmt::mp("in"));
		set_msg_handler(pmt::mp("in"),
				boost::bind(&c_time_sink_impl::handle_pdus, this, _1));

		// +2 for the PDU message buffers
		for(int n = 0; n < d_nconnections+2; n++) {
			d_buffers.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
					volk_get_alignment()));
			memset(d_buffers[n], 0, d_buffer_size*sizeof(double));
		}

		// We don't use cbuffers with the PDU message handling capabilities.
		for(int n = 0; n < d_nconnections/2; n++) {
			d_cbuffers.push_back((gr_complex*)volk_malloc(d_buffer_size*sizeof(gr_complex),
					volk_get_alignment()));
			memset(d_cbuffers[n], 0, d_buffer_size*sizeof(gr_complex));
		}

		for(int i = 0; i < d_nconnections; i++) {
			d_residbufs_real.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
					volk_get_alignment()));
			d_residbufs_imag.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
					volk_get_alignment()));
			memset(d_residbufs_real[i], 0, d_buffer_size*sizeof(double));
			memset(d_residbufs_imag[i], 0, d_buffer_size*sizeof(double));
		}

		// Used for PDU message input
		d_residbufs_real.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
				volk_get_alignment()));
		d_residbufs_imag.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
				volk_get_alignment()));
		memset(d_residbufs_real[d_nconnections], 0, d_buffer_size*sizeof(double));
		memset(d_residbufs_imag[d_nconnections], 0, d_buffer_size*sizeof(double));


		// Set alignment properties for VOLK
		const int alignment_multiple =
				volk_get_alignment() / sizeof(gr_complex);
		set_alignment(std::max(1,alignment_multiple));

		d_tags = std::vector< std::vector<gr::tag_t> >(d_nconnections/2);

		initialize();

		// d_main_gui->setNPoints(d_size); // setup GUI box with size
		set_trigger_mode(TRIG_MODE_FREE, TRIG_SLOPE_POS, 0, 0, 0);

		set_history(2);          // so we can look ahead for the trigger slope
		declare_sample_delay(1); // delay the tags for a history of 2

	}

	/*
	 * Our virtual destructor.
	 */
	c_time_sink_impl::~c_time_sink_impl()
	{
		// d_main_gui is a qwidget destroyed with its parent
		for(int n = 0; n < d_nconnections+2; n++) {
			volk_free(d_buffers[n]);
		}
		for(int n = 0; n < d_nconnections/2; n++) {
			volk_free(d_cbuffers[n]);
		}
		for(int i = 0; i < d_nconnections+1; i++) {
			volk_free(d_residbufs_real[i]);
			volk_free(d_residbufs_imag[i]);
		}
	}

	void
	c_time_sink_impl::initialize()
	{
		int numplots = (d_nconnections > 0) ? d_nconnections : 2;

		// initialize update time to 10 times a second
		set_update_time(0.1);
	}

	void
	c_time_sink_impl::set_update_time(double t)
	{
		//convert update time to ticks
		gr::high_res_timer_type tps = gr::high_res_timer_tps();
		d_update_time = t * tps;
		//    d_main_gui->setUpdateTime(t);
		d_last_time = 0;
	}

	void
	c_time_sink_impl::set_trigger_mode(trigger_mode mode,
			trigger_slope slope,
			float level,
			float delay, int channel,
			const std::string &tag_key)
	{
		gr::thread::scoped_lock lock(d_setlock);

		d_trigger_mode = mode;
		d_trigger_slope = slope;
		d_trigger_level = level;
		d_trigger_delay = static_cast<int>(delay*d_samp_rate);
		d_trigger_channel = channel;
		d_trigger_tag_key = pmt::intern(tag_key);
		d_triggered = false;
		d_trigger_count = 0;

		if((d_trigger_delay < 0) || (d_trigger_delay >= d_size)) {
			GR_LOG_WARN(d_logger, boost::format("Trigger delay (%1%) outside of display range (0:%2%).") \
					% (d_trigger_delay/d_samp_rate) % ((d_size-1)/d_samp_rate));
			d_trigger_delay = std::max(0, std::min(d_size-1, d_trigger_delay));
			delay = d_trigger_delay/d_samp_rate;
		}

		_reset();
	}

	void
	c_time_sink_impl::set_nsamps(const int newsize)
	{
		if(newsize != d_size) {
			gr::thread::scoped_lock lock(d_setlock);

			// Set new size and reset buffer index
			// (throws away any currently held data, but who cares?)
			d_size = newsize;
			d_buffer_size = 2*d_size;

			// Resize buffers and replace data
			for(int n = 0; n < d_nconnections+2; n++) {
				volk_free(d_buffers[n]);
				d_buffers[n] = (double*)volk_malloc(d_buffer_size*sizeof(double),
						volk_get_alignment());
				memset(d_buffers[n], 0, d_buffer_size*sizeof(double));
			}

			for(int n = 0; n < d_nconnections/2; n++) {
				volk_free(d_cbuffers[n]);
				d_cbuffers[n] = (gr_complex*)volk_malloc(d_buffer_size*sizeof(gr_complex),
						volk_get_alignment());
				memset(d_cbuffers[n], 0, d_buffer_size*sizeof(gr_complex));
			}

			// Resize residbuf and replace data
			// +1 to handle PDU message input buffers
			for(int i = 0; i < d_nconnections+1; i++) {
				volk_free(d_residbufs_real[i]);
				volk_free(d_residbufs_imag[i]);
				d_residbufs_real[i] = (double*)volk_malloc(d_buffer_size*sizeof(double),
						volk_get_alignment());
				d_residbufs_imag[i] = (double*)volk_malloc(d_buffer_size*sizeof(double),
						volk_get_alignment());

				memset(d_residbufs_real[i], 0, d_buffer_size*sizeof(double));
				memset(d_residbufs_imag[i], 0, d_buffer_size*sizeof(double));
			}
			// If delay was set beyond the new boundary, pull it back.
			if(d_trigger_delay >= d_size) {
				GR_LOG_WARN(d_logger, boost::format("Trigger delay (%1%) outside of display range (0:%2%). Moving to 50%% point.") \
						% (d_trigger_delay/d_samp_rate) % ((d_size-1)/d_samp_rate));
				d_trigger_delay = d_size/2;
				//      d_main_gui->setTriggerDelay(d_trigger_delay/d_samp_rate);
			}

			//d_main_gui->setNPoints(d_size);
			_reset();
		}
	}

	void
	c_time_sink_impl::set_samp_rate(const double samp_rate)
	{
		gr::thread::scoped_lock lock(d_setlock);
		d_samp_rate = samp_rate;
		//   d_main_gui->setSampleRate(d_samp_rate);
	}

	int
	c_time_sink_impl::nsamps() const
	{
		return d_size;
	}


	//        void
	//            time_sink_c_impl::reset()
	//            {
	//              gr::thread::scoped_lock lock(d_setlock);
	//              _reset();
	//            }

	void
	c_time_sink_impl::_reset()
	{
		int n;
		if(d_trigger_delay) {
			for(n = 0; n < d_nconnections/2; n++) {
				// Move the tail of the buffers to the front. This section
				// represents data that might have to be plotted again if a
				// trigger occurs and we have a trigger delay set.  The tail
				// section is between (d_end-d_trigger_delay) and d_end.
				memmove(d_cbuffers[n], &d_cbuffers[n][d_end-d_trigger_delay],
						d_trigger_delay*sizeof(gr_complex));

				// Also move the offsets of any tags that occur in the tail
				// section so they would be plotted again, too.
				std::vector<gr::tag_t> tmp_tags;
				for(size_t t = 0; t < d_tags[n].size(); t++) {
					if(d_tags[n][t].offset > (uint64_t)(d_size - d_trigger_delay)) {
						d_tags[n][t].offset = d_tags[n][t].offset - (d_size - d_trigger_delay);
						tmp_tags.push_back(d_tags[n][t]);
					}
				}
				d_tags[n] = tmp_tags;
			}
		}
		// Otherwise, just clear the local list of tags.
		else {
			for(n = 0; n < d_nconnections/2; n++) {
				d_tags[n].clear();
			}
		}

		// Reset the start and end indices.
		d_start = 0;
		d_end = d_size;

		// Reset the trigger. If in free running mode, ignore the
		// trigger delay and always set trigger to true.
		if(d_trigger_mode == TRIG_MODE_FREE) {
			d_index = 0;
			d_triggered = true;
		}
		else {
			d_index = d_trigger_delay;
			d_triggered = false;
		}
	}

	void
	c_time_sink_impl::_npoints_resize()
	{
		// int newsize = d_main_gui->getNPoints();
		int newsize = d_size;
		set_nsamps(newsize);
	}

	void
	c_time_sink_impl::_adjust_tags(int adj)
	{
		for(size_t n = 0; n < d_tags.size(); n++) {
			for(size_t t = 0; t < d_tags[n].size(); t++) {
				d_tags[n][t].offset += adj;
			}
		}
	}

	void
	c_time_sink_impl::_test_trigger_tags(int nitems)
	{
		int trigger_index;

		uint64_t nr = nitems_read(d_trigger_channel/2);
		std::vector<gr::tag_t> tags;
		get_tags_in_range(tags, d_trigger_channel/2,
				nr, nr + nitems + 1,
				d_trigger_tag_key);
		if(tags.size() > 0) {
			d_triggered = true;
			trigger_index = tags[0].offset - nr;
			d_start = d_index + trigger_index - d_trigger_delay - 1;
			d_end = d_start + d_size;
			d_trigger_count = 0;
			_adjust_tags(-d_start);
		}
	}

	void
	c_time_sink_impl::_test_trigger_norm(int nitems, gr_vector_const_void_star inputs)
	{
		int trigger_index;
		const gr_complex *in = (const gr_complex*)inputs[d_trigger_channel/2];
		for(trigger_index = 0; trigger_index < nitems; trigger_index++) {
			d_trigger_count++;

			// Test if trigger has occurred based on the input stream,
			// channel number, and slope direction
			if(_test_trigger_slope(&in[trigger_index])) {
				d_triggered = true;
				d_start = d_index + trigger_index - d_trigger_delay;
				d_end = d_start + d_size;
				d_trigger_count = 0;
				_adjust_tags(-d_start);
				break;
			}
		}

		// If using auto trigger mode, trigger periodically even
		// without a trigger event.
		if((d_trigger_mode == TRIG_MODE_AUTO) && (d_trigger_count > d_size)) {
			d_triggered = true;
			d_trigger_count = 0;
		}
	}

	bool
	c_time_sink_impl::_test_trigger_slope(const gr_complex *in) const
	{
		float x0, x1;
		if(d_trigger_channel % 2 == 0) {
			x0 = in[0].real();
			x1 = in[1].real();
		}
		else {
			x0 = in[0].imag();
			x1 = in[1].imag();
		}

		if(d_trigger_slope == TRIG_SLOPE_POS)
			return ((x0 <= d_trigger_level) && (x1 > d_trigger_level));
		else
			return ((x0 >= d_trigger_level) && (x1 < d_trigger_level));
	}

	void
	c_time_sink_impl::_gui_update_trigger()
	{
		//	      d_trigger_mode = d_main_gui->getTriggerMode();
		//	      d_trigger_slope = d_main_gui->getTriggerSlope();
		//	      d_trigger_level = d_main_gui->getTriggerLevel();
		//	      d_trigger_channel = d_main_gui->getTriggerChannel();
		d_trigger_count = 0;

		float delayf = 0;
		int delay = static_cast<int>(delayf*d_samp_rate);

		if(delay != d_trigger_delay) {
			// We restrict the delay to be within the window of time being
			// plotted.
			if((delay < 0) || (delay >= d_size)) {
				GR_LOG_WARN(d_logger, boost::format("Trigger delay (%1%) outside of display range (0:%2%).") \
						% (delay/d_samp_rate) % ((d_size-1)/d_samp_rate));
				delay = std::max(0, std::min(d_size-1, delay));
				delayf = delay/d_samp_rate;
			}

			d_trigger_delay = delay;
			///  d_main_gui->setTriggerDelay(delayf);
			_reset();
		}

		//  std::string tagkey = d_main_gui->getTriggerTagKey();
		// d_trigger_tag_key = pmt::intern(tagkey);
	}

	int
	c_time_sink_impl::work(int noutput_items,
			gr_vector_const_void_star &input_items,
			gr_vector_void_star &output_items)
	{
		int n=0;
		const gr_complex *in;

		_npoints_resize();
		_gui_update_trigger();

		gr::thread::scoped_lock lock(d_setlock);

		int nfill = d_end - d_index;                 // how much room left in buffers
		int nitems = std::min(noutput_items, nfill); // num items we can put in buffers

		// If auto, normal, or tag trigger, look for the trigger
		if((d_trigger_mode != TRIG_MODE_FREE) && !d_triggered) {
			// trigger off a tag key (first one found)
			if(d_trigger_mode == TRIG_MODE_TAG) {
				_test_trigger_tags(nitems);
			}
			// Normal or Auto trigger
			else {
				_test_trigger_norm(nitems, input_items);
			}
		}

		// Copy data into the buffers.
		for(n = 0; n < d_nconnections/2; n++) {
			in = (const gr_complex*)input_items[n];
			memcpy(&d_cbuffers[n][d_index], &in[1], nitems*sizeof(gr_complex));

			uint64_t nr = nitems_read(n);
			std::vector<gr::tag_t> tags;
			get_tags_in_range(tags, n, nr, nr + nitems);
			for(size_t t = 0; t < tags.size(); t++) {
				tags[t].offset = tags[t].offset - nr + (d_index-d_start-1);
			}
			d_tags[n].insert(d_tags[n].end(), tags.begin(), tags.end());
		}
		d_index += nitems;

		// If we've have a trigger and a full d_size of items in the buffers, plot.
		if((d_triggered) && (d_index == d_end)) {
			// Copy data to be plotted to start of buffers.
			for(n = 0; n < d_nconnections/2; n++) {
				volk_32fc_deinterleave_64f_x2(d_buffers[2*n+0], d_buffers[2*n+1],
						&d_cbuffers[n][d_start], d_size);
			}

			double axis = 10/(double)d_index;
			// Copy data to be plotted to start of buffers.
			for(n = 0; n < d_nconnections; n++) {
				memmove(d_residbufs_real[n], &d_buffers[n][2*n+0], d_size*sizeof(double));
				memmove(d_residbufs_imag[n], &d_buffers[n][2*n+1], d_size*sizeof(double));
				for(int64_t point = 0; point < d_size; point++) {
					double xaxis= axis*(double)point;
					int result = oml_inject_timeplotmp(
							g_oml_mps_APPNAME->timeplotmp,
							(double)d_residbufs_real[n][point],
							(double)d_residbufs_imag[n][point],
							sqrt ((pow ((double)d_residbufs_real[n][point], 2)) * pow ((double)d_residbufs_imag[n][point] , 2)),
							atan((double)d_residbufs_imag[n][point]/(double)d_residbufs_real[n][point]) ,
							(double)xaxis);
					if (result == -1) {
						fprintf (stderr, "Could not send waterfall data to OML\n");
					}
//					printf("[real-r] = %f  n %f \n", (double)d_residbufs_real[n][point], xaxis);
//					printf("[imag-i] %f point %f \n",  (double)d_residbufs_imag[n][point], xaxis);
				}
			}

			// Plot if we are able to update
			if(gr::high_res_timer_now() - d_last_time > d_update_time) {
				d_last_time = gr::high_res_timer_now();
				//    	          d_qApplication->postEvent(d_main_gui,
				//    	                                    new TimeUpdateEvent(d_buffers, d_size, d_tags));
			}

			// We've plotting, so reset the state
			_reset();
		}

		// If we've filled up the buffers but haven't triggered, reset.
		if(d_index == d_end) {
			_reset();
		}

		return nitems;
	}


	void
	c_time_sink_impl::handle_pdus(pmt::pmt_t msg)
	{
		size_t len;
		pmt::pmt_t dict, samples;
		std::vector< std::vector<gr::tag_t> > t(1);

		// Test to make sure this is either a PDU or a uniform vector of
		// samples. Get the samples PMT and the dictionary if it's a PDU.
		// If not, we throw an error and exit.
		if(pmt::is_pair(msg)) {
			dict = pmt::car(msg);
			samples = pmt::cdr(msg);
		}
		else if(pmt::is_uniform_vector(msg)) {
			samples = msg;
		}
		else {
			throw std::runtime_error("time_sink_c: message must be either "
					"a PDU or a uniform vector of samples.");
		}

		// add tag info if it is present in metadata
		if(pmt::is_dict(dict)){
			if(pmt::dict_has_key(dict, pmt::mp("tags"))){
				d_tags.clear();
				pmt::pmt_t tags = pmt::dict_ref(dict, pmt::mp("tags"), pmt::PMT_NIL);
				int len = pmt::length(tags);
				for(int i=0; i<len; i++){
					// get tag info from list
					pmt::pmt_t tup = pmt::vector_ref(tags, i);
					int tagval = pmt::to_long(pmt::tuple_ref(tup,0));
					pmt::pmt_t k = pmt::tuple_ref(tup,1);
					pmt::pmt_t v = pmt::tuple_ref(tup,2);

					// add the tag
					t[0].push_back( gr::tag_t() );
					t[0][t[0].size()-1].offset = tagval;
					t[0][t[0].size()-1].key = k;
					t[0][t[0].size()-1].value = v;
					t[0][t[0].size()-1].srcid = pmt::PMT_NIL;
				}
			}
		}

		len = pmt::length(samples);

		const gr_complex *in;
		if(pmt::is_c32vector(samples)) {
			in = (const gr_complex*)pmt::c32vector_elements(samples, len);
		}
		else {
			throw std::runtime_error("time_sink_c: unknown data type "
					"of samples; must be complex.");
		}

		// Plot if we're past the last update time
		if(gr::high_res_timer_now() - d_last_time > d_update_time) {
			d_last_time = gr::high_res_timer_now();

			set_nsamps(len);

			volk_32fc_deinterleave_64f_x2(d_buffers[2*d_nconnections+0],
					d_buffers[2*d_nconnections+1],
					in, len);

			//            d_qApplication->postEvent(d_main_gui,
			//                                      new TimeUpdateEvent(d_buffers, len, t));
		}
	}

	} /* namespace omlforge */
	} /* namespace gr */

