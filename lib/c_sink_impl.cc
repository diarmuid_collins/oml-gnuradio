	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifdef HAVE_CONFIG_H
	#include "config.h"
	#endif

	#include <gnuradio/io_signature.h>
	#include "c_sink_impl.h"
	#include <volk/volk.h>
	#include <stdio.h>
	#include <oml2/omlc.h>
	#define OML_FROM_MAIN/* IMPORTANT: instruct APPNAME_oml.h to enable the generated code, rather than just declaring prototypes; do this in only one source file */
	#include <APPNAME_oml.h>

	namespace gr {
	namespace omlforge {

	c_sink::sptr
	c_sink::make()
	{
		return gnuradio::get_initial_sptr
				(new c_sink_impl());
	}

	/*
	 * The private constructor
	 */
	c_sink_impl::c_sink_impl()
	: gr::sync_block("c_sink",
			gr::io_signature::make(0, 1, sizeof(gr_complex)), // input signature
			gr::io_signature::make(0, 0, 0)) // output signature
	{

		//initialize oml
		initialize();
	}

	/*
	 * Our virtual destructor.
	 */
	c_sink_impl::~c_sink_impl()
	{
	}

	void
	c_sink_impl::initialize()
	{

		/* OML Stuff*/
		int argc=2;
		static const char * args[] = {"--oml-config", "/home/nodeuser/oml-config.xml"};
		int result = omlc_init ("GNURadioVM", &argc, args, NULL);

		if (result == -1) {
			fprintf (stderr, "Could not initialize OML\n");
			exit (1);
		}

		oml_register_mps();

		result = omlc_start();

		if (result == -1) {
			fprintf (stderr, "Error starting up OML measurement streams\n");
			exit (1);
		}

	}

	int
	c_sink_impl::work(int noutput_items,
			gr_vector_const_void_star &input_items,
			gr_vector_void_star &output_items)
	{
		const gr_complex *in = (const gr_complex*)input_items[0];

		// Do <+signal processing+>
		// Tell runtime system how many input items we consumed on
		// each input stream.
		consume_each (noutput_items);

		// Tell runtime system how many output items we produced.
		return noutput_items;
	}

	} /* namespace omlforge */
	} /* namespace gr */

