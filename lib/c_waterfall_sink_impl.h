	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifndef INCLUDED_OMLFORGE_C_WATERFALL_SINK_IMPL_H
	#define INCLUDED_OMLFORGE_C_WATERFALL_SINK_IMPL_H

	#include <omlforge/c_waterfall_sink.h>
	#include <gnuradio/fft/fft.h>
	#include <gnuradio/high_res_timer.h>

	namespace gr {
	namespace omlforge {

	class c_waterfall_sink_impl : public c_waterfall_sink
	{
	private:
		int d_fftsize;
		fft::fft_complex *d_fft;
		void initialize();
		float d_fftavg; //fft alpha average

		gr::high_res_timer_type d_update_time;
		gr::high_res_timer_type d_last_time;

		int d_nconnections;
		int d_index;

		int d_nrows;
		bool d_shift;



		std::vector<gr_complex*> d_residbufs;
		std::vector<double*> d_magbufs;
		double* d_pdu_magbuf;
		float *d_fbuf;

		double d_center_freq ;//fc : center frequency of signal (use for x-axis labels)
		double d_bandwidth;  //bw : bandwidth of signal (used to set x-axis labels)
		double d_units;
		double d_samp_rate;

		void fft(float *data_out, const gr_complex *data_in, int size);

		// Handles message input port for displaying PDU samples.
		void handle_pdus(pmt::pmt_t msg);

		// Handles message input port for setting new center frequency.
		// The message is a PMT pair (intern('freq'), double(frequency)).
		void handle_set_freq(pmt::pmt_t msg);

	public:
		c_waterfall_sink_impl();
		~c_waterfall_sink_impl();

		void set_fft_size(const int fftsize);
		int fft_size() const;
		void set_fft_average(const float fftavg);
		float fft_average() const;

		void set_frequency_range(const double centerfreq, const double bandwidth);
		// Where all the action really happens
		void set_update_time(double t);

		void fftresize();

		// Where all the action really happens
		int work(int noutput_items,
				gr_vector_const_void_star &input_items,
				gr_vector_void_star &output_items);
	};

	} // namespace omlforge
	} // namespace gr

	#endif /* INCLUDED_OMLFORGE_C_WATERFALL_SINK_IMPL_H */

