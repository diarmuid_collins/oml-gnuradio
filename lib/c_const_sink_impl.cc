	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifdef HAVE_CONFIG_H
	#include "config.h"
	#endif

	#include <gnuradio/io_signature.h>
	#include "c_const_sink_impl.h"
	#include <gnuradio/prefs.h>
	#include <string.h>
	#include <stdio.h>
	#include <volk/volk.h>
	#include <oml2/omlc.h>
	//#define OML_FROM_MAIN /* IMPORTANT: instruct APPNAME_oml.h to enable the generated code, rather than just declaring prototypes; do this in only one source file */
	#include <APPNAME_oml.h>

	namespace gr {
	namespace omlforge {

	c_const_sink::sptr
	c_const_sink::make()
	{
		return gnuradio::get_initial_sptr
				(new c_const_sink_impl());
	}

	/*
	 * The private constructor
	 */
	c_const_sink_impl::c_const_sink_impl()
	: gr::sync_block("c_const_sink",
			gr::io_signature::make(0, 1, sizeof(gr_complex)), // input signature
			gr::io_signature::make(0, 0, 0)) // output signature
	{d_size = 1024; // fftsize : size of the FFT to compute and display. If using the PDU message port to plot samples, the length of each PDU must be a multiple of the FFT size.
	d_nconnections = 1; //nconnections : number of signals to be connected to the sink. The PDU message port is always available for a connection, and this value must be set to 0 if only the PDU message port is being used.
	d_index = 0;
	d_end = d_size;

	d_index = 0;

	// setup PDU handling input port
	message_port_register_in(pmt::mp("in"));
	set_msg_handler(pmt::mp("in"),
			boost::bind(&c_const_sink_impl::handle_pdus, this, _1));

	for(int i = 0; i < d_nconnections; i++) {
		d_residbufs_real.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
				volk_get_alignment()));
		d_residbufs_imag.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
				volk_get_alignment()));
		memset(d_residbufs_real[i], 0, d_buffer_size*sizeof(double));
		memset(d_residbufs_imag[i], 0, d_buffer_size*sizeof(double));
	}

	// Used for PDU message input
	d_residbufs_real.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
			volk_get_alignment()));
	d_residbufs_imag.push_back((double*)volk_malloc(d_buffer_size*sizeof(double),
			volk_get_alignment()));
	memset(d_residbufs_real[d_nconnections], 0, d_buffer_size*sizeof(double));
	memset(d_residbufs_imag[d_nconnections], 0, d_buffer_size*sizeof(double));

	// Set alignment properties for VOLK
	const int alignment_multiple =
			volk_get_alignment() / sizeof(gr_complex);
	set_alignment(std::max(1,alignment_multiple));

	initialize();

	set_trigger_mode(TRIG_MODE_FREE, TRIG_SLOPE_POS, 0, 0);

	set_history(2);          // so we can look ahead for the trigger slope
	declare_sample_delay(1); // delay the tags for a history of 2


	}

	/*
	 * Our virtual destructor.
	 */
	c_const_sink_impl::~c_const_sink_impl()
	{
		for(int i = 0; i < d_nconnections+1; i++) {
			volk_free(d_residbufs_real[i]);
			volk_free(d_residbufs_imag[i]);
		}
	}


	void
	c_const_sink_impl::initialize()
	{
		int numplots = (d_nconnections > 0) ? d_nconnections : 1;
		d_last_time = 0;
		// initialize update time to 10 times a second
		set_update_time(0.1);
	}

	void
	c_const_sink_impl::set_update_time(double t)
	{
		//convert update time to ticks
		gr::high_res_timer_type tps = gr::high_res_timer_tps();
		d_update_time = t * tps;

		d_last_time = 0;
	}

	void
	c_const_sink_impl::set_trigger_mode(trigger_mode mode,
			trigger_slope slope,
			float level,
			int channel,
			const std::string &tag_key)
	{
		gr::thread::scoped_lock lock(d_setlock);

		d_trigger_mode = mode;
		d_trigger_slope = slope;
		d_trigger_level = level;
		d_trigger_channel = channel;
		d_trigger_tag_key = pmt::intern(tag_key);
		d_triggered = false;
		d_trigger_count = 0;


		_reset();
	}

	void
	c_const_sink_impl::set_nsamps(const int newsize)
	{
		gr::thread::scoped_lock lock(d_setlock);

		if(newsize != d_size) {
			// Set new size and reset buffer index
			// (throws away any currently held data, but who cares?)
			d_size = newsize;
			d_buffer_size = 2*d_size;
			d_index = 0;

			// Resize residbuf and replace data
			// +1 to handle PDU message input buffers
			for(int i = 0; i < d_nconnections+1; i++) {
				volk_free(d_residbufs_real[i]);
				volk_free(d_residbufs_imag[i]);
				d_residbufs_real[i] = (double*)volk_malloc(d_buffer_size*sizeof(double),
						volk_get_alignment());
				d_residbufs_imag[i] = (double*)volk_malloc(d_buffer_size*sizeof(double),
						volk_get_alignment());

				memset(d_residbufs_real[i], 0, d_buffer_size*sizeof(double));
				memset(d_residbufs_imag[i], 0, d_buffer_size*sizeof(double));
			}

			//d_main_gui->setNPoints(d_size);
			_reset();
		}
	}

	int
	c_const_sink_impl::nsamps() const
	{
		return d_size;
	}


	void
	c_const_sink_impl::reset()
	{
		gr::thread::scoped_lock lock(d_setlock);
		_reset();
	}

	void
	c_const_sink_impl::_reset()
	{
		// Reset the start and end indices.
		d_start = 0;
		d_end = d_size;
		d_index = 0;

		// Reset the trigger.
		if(d_trigger_mode == TRIG_MODE_FREE) {
			d_triggered = true;
		}
		else {
			d_triggered = false;
		}
	}

	void
	c_const_sink_impl::_npoints_resize()
	{
		int newsize = d_size;
		set_nsamps(newsize);
	}

	void
	c_const_sink_impl::_test_trigger_tags(int nitems)
	{
		int trigger_index;

		uint64_t nr = nitems_read(d_trigger_channel);
		std::vector<gr::tag_t> tags;
		get_tags_in_range(tags, d_trigger_channel,
				nr, nr + nitems,
				d_trigger_tag_key);
		if(tags.size() > 0) {
			d_triggered = true;
			trigger_index = tags[0].offset - nr;
			d_start = d_index + trigger_index - 1;
			d_end = d_start + d_size;
			d_trigger_count = 0;
		}
	}

	void
	c_const_sink_impl::_test_trigger_norm(int nitems, gr_vector_const_void_star inputs)
	{
		int trigger_index;
		const gr_complex *in = (const gr_complex*)inputs[d_trigger_channel];
		for(trigger_index = 0; trigger_index < nitems; trigger_index++) {
			d_trigger_count++;

			// Test if trigger has occurred based on the input stream,
			// channel number, and slope direction
			if(_test_trigger_slope(&in[trigger_index])) {
				d_triggered = true;
				d_start = d_index + trigger_index;
				d_end = d_start + d_size;
				d_trigger_count = 0;
				break;
			}
		}

		// If using auto trigger mode, trigger periodically even
		// without a trigger event.
		if((d_trigger_mode == TRIG_MODE_AUTO) && (d_trigger_count > d_size)) {
			d_triggered = true;
			d_trigger_count = 0;
		}
	}

	bool
	c_const_sink_impl::_test_trigger_slope(const gr_complex *in) const
	{
		float x0, x1;
		x0 = abs(in[0]);
		x1 = abs(in[1]);

		if(d_trigger_slope == TRIG_SLOPE_POS)
			return ((x0 <= d_trigger_level) && (x1 > d_trigger_level));
		else
			return ((x0 >= d_trigger_level) && (x1 < d_trigger_level));
	}

//	void
//	c_const_sink_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
//	{
//		//		unsigned ninputs = ninput_items_required.size();
//		//		    for(unsigned i = 0; i < ninputs; i++)
//		//		      ninput_items_required[i] = fixed_rate_noutput_to_ninput(noutput_items);
//	}
//
//	//	int
//	//	constellation_sink_impl::fixed_rate_noutput_to_ninput(int noutput_items)
//	//	  {
//	//	    return noutput_items + history() - 1;
//	//	  }



	int
	c_const_sink_impl::work(int noutput_items,
			gr_vector_const_void_star &input_items,
			gr_vector_void_star &output_items)
	{

		int n=0;
		const gr_complex *in;

		_npoints_resize();
		// _gui_update_trigger();

		int nfill = d_end - d_index;                 // how much room left in buffers
		int nitems = std::min(noutput_items, nfill); // num items we can put in buffers

		// If auto, normal, or tag trigger, look for the trigger
		if((d_trigger_mode != TRIG_MODE_FREE) && !d_triggered) {
			// trigger off a tag key (first one found)
			if(d_trigger_mode == TRIG_MODE_TAG) {
				_test_trigger_tags(nitems);
			}
			// Normal or Auto trigger
			else {
				_test_trigger_norm(nitems, input_items);
			}
		}

		// Copy data into the buffers.
		for(n = 0; n < d_nconnections; n++) {
			in = (const gr_complex*)input_items[n];
			volk_32fc_deinterleave_64f_x2(&d_residbufs_real[n][d_index],
					&d_residbufs_imag[n][d_index],
					&in[0], nitems);
		}
		d_index += nitems;


		// If we have a trigger and a full d_size of items in the buffers, plot.
		if((d_triggered) && (d_index == d_end)) {
			// Copy data to be plotted to start of buffers.
			for(n = 0; n < d_nconnections; n++) {
				memmove(d_residbufs_real[n], &d_residbufs_real[n][d_start], d_size*sizeof(double));
				memmove(d_residbufs_imag[n], &d_residbufs_imag[n][d_start], d_size*sizeof(double));
				for(int64_t point = 0; point < d_size; point++) {
					int result = oml_inject_constplotmp(
							g_oml_mps_APPNAME->constplotmp, d_residbufs_real[n][point],
							d_residbufs_imag[n][point]);
					if (result == -1) {
						fprintf (stderr, "Could not send to OML\n");
					}
				//	printf("[real-r] = %f  n %f \n", d_residbufs_real[n][point], n);
				//	printf("[imag-i] %f point %f \n",  d_residbufs_imag[n][point], point);
				}
			}

			// Plot if we are able to update
			if(gr::high_res_timer_now() - d_last_time > d_update_time) {
				d_last_time = gr::high_res_timer_now();
				//    	          d_qApplication->postEvent(d_main_gui,
				//    	                                    new ConstUpdateEvent(d_residbufs_real,
				//    								 d_residbufs_imag,
				//    								 d_size));
			}

			// We've plotting, so reset the state
			_reset();
		}

		// If we've filled up the buffers but haven't triggered, reset.
		if(d_index == d_end) {
			_reset();
		}

		return nitems;
	}



	void
	c_const_sink_impl::handle_pdus(pmt::pmt_t msg)
	{
		size_t len = 0;
		pmt::pmt_t dict, samples;

		// Test to make sure this is either a PDU or a uniform vector of
		// samples. Get the samples PMT and the dictionary if it's a PDU.
		// If not, we throw an error and exit.
		if(pmt::is_pair(msg)) {
			dict = pmt::car(msg);
			samples = pmt::cdr(msg);
		}
		else if(pmt::is_uniform_vector(msg)) {
			samples = msg;
		}
		else {
			throw std::runtime_error("const_sink_c: message must be either "
					"a PDU or a uniform vector of samples.");
		}

		len = pmt::length(samples);

		const gr_complex *in;
		if(pmt::is_c32vector(samples)) {
			in = (const gr_complex*)pmt::c32vector_elements(samples, len);
		}
		else {
			throw std::runtime_error("const_sink_c: unknown data type "
					"of samples; must be complex.");
		}

		set_nsamps(len);

		// Plot if we're past the last update time
		if(gr::high_res_timer_now() - d_last_time > d_update_time) {
			d_last_time = gr::high_res_timer_now();

			// Copy data into the buffers.
			volk_32fc_deinterleave_64f_x2(d_residbufs_real[d_nconnections],
					d_residbufs_imag[d_nconnections],
					in, len);

			//            d_qApplication->postEvent(d_main_gui,
			//                                      new ConstUpdateEvent(d_residbufs_real,
			//                                                           d_residbufs_imag,
			//                                                           len));
		}
	}



	} /* namespace omlforge */
	} /* namespace gr */

