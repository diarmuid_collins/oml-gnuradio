	/* -*- c++ -*- */
	/*
	 * Copyright 2016 <+YOU OR YOUR COMPANY+>.
	 *
	 * This is free software; you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation; either version 3, or (at your option)
	 * any later version.
	 *
	 * This software is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this software; see the file COPYING.  If not, write to
	 * the Free Software Foundation, Inc., 51 Franklin Street,
	 * Boston, MA 02110-1301, USA.
	 */

	#ifdef HAVE_CONFIG_H
	#include "config.h"
	#endif

	#include <gnuradio/io_signature.h>
	#include "c_waterfall_null_impl.h"
	#include <stdio.h>
	#include <time.h>
	#include <oml2/omlc.h>
	//#define OML_FROM_MAIN /* IMPORTANT: instruct APPNAME_oml.h to enable the generated code, rather than just declaring prototypes; do this in only one source file */
	#include <APPNAME_oml.h>

	namespace gr {
	namespace omlforge {

	c_waterfall_null::sptr
	c_waterfall_null::make()
	{
		return gnuradio::get_initial_sptr
				(new c_waterfall_null_impl());
	}

	/*
	 * The private constructor
	 */
	c_waterfall_null_impl::c_waterfall_null_impl()
	: gr::sync_block("c_waterfall_null",
			gr::io_signature::make(0, 1, sizeof(gr_complex)),
			gr::io_signature::make(0, 0, 0))
	{
		d_update_time = 0;
	}

	/*
	 * Our virtual destructor.
	 */
	c_waterfall_null_impl::~c_waterfall_null_impl()
	{
	}

	int
	c_waterfall_null_impl::work(int noutput_items,
			gr_vector_const_void_star &input_items,
			gr_vector_void_star &output_items)
	{
		int n=0;
		const gr_complex *in;

		time_t     now;
	    // Get current time
	    time(&now);
	    //printf("Load waterfall null data sent %lu \n", now);
		if (now >= d_update_time ){
			d_update_time = now + 10;
			printf("Load waterfall null data sent %lu \n", now);
			//insert some junk data into OML database so we can differentiate between new and old packet data in waterfall graph.
			for(int x = 0; x < 2000; x++) {
				int result = oml_inject_waterfallplotmp(
						g_oml_mps_APPNAME->waterfallplotmp, 0,
						gr::high_res_timer_now(), 11);
				if (result == -1) {
					fprintf (stderr, "Could not send waterfall data to OML\n");
				}
				x++;
			}
		}
		// Do <+signal processing+>

		// Tell runtime system how many output items we produced.
		return noutput_items;
	}

	} /* namespace omlforge */
	} /* namespace gr */

