#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: OFDM Tx
# Description: Example of an OFDM Transmitter
# Generated: Thu May 19 12:49:06 2016
##################################################

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.digital.utils import tagged_streams
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import SimpleXMLRPCServer
import numpy
import random
import sip
import sys
import threading
import time

class tv13x_ofdm(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "OFDM Tx")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("OFDM Tx")
        try:
             self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
             pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "tv13x_ofdm")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.payloadmodfrnt = payloadmodfrnt = 'digital.constellation_8psk()'
        self.occupied_carriers = occupied_carriers = (range(-26, -21) + range(-20, -7) + range(-6, 0) + range(1, 7) + range(8, 21) + range(22, 27),)
        self.length_tag_key = length_tag_key = "packet_len"
        self.bandwidth = bandwidth = 200000
        self.sync_word2 = sync_word2 = [0, 0, 0, 0, 0, 0, -1, -1, -1, -1, 1, 1, -1, -1, -1, 1, -1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, 0, 1, -1, 1, 1, 1, -1, 1, 1, 1, -1, 1, 1, 1, 1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, -1, -1, 0, 0, 0, 0, 0] 
        self.sync_word1 = sync_word1 = [0., 0., 0., 0., 0., 0., 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., -1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., -1.41421356, 0., -1.41421356, 0., -1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 1.41421356, 0., -1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 1.41421356, 0., 0., 0., 0., 0., 0.]
        self.samp_rate = samp_rate = bandwidth
        self.rolloff = rolloff = 0
        self.pilot_symbols = pilot_symbols = ((1, 1, 1, -1,),)
        self.pilot_carriers = pilot_carriers = ((-21, -7, 7, 21,),)
        self.payloadmod = payloadmod = eval(str(payloadmodfrnt).replace("'",""))
        self.packetlen = packetlen = 96
        self.ipaddress = ipaddress = "192.168.5.113"
        self.header_mod = header_mod = digital.constellation_bpsk()
        self.header_formatter = header_formatter = digital.packet_header_ofdm(occupied_carriers, 1, length_tag_key)
        self.gain = gain = 15
        self.freq = freq = 2490500000
        self.cycleprefix = cycleprefix = 16

        ##################################################
        # Blocks
        ##################################################
        self._payloadmod_tool_bar = Qt.QToolBar(self)
        self._payloadmod_tool_bar.addWidget(Qt.QLabel("payloadmod"+": "))
        self._payloadmod_line_edit = Qt.QLineEdit(str(self.payloadmod))
        self._payloadmod_tool_bar.addWidget(self._payloadmod_line_edit)
        self._payloadmod_line_edit.returnPressed.connect(
        	lambda: self.set_payloadmod(eval(self._payloadmod_line_edit.text().toAscii())))
        self.top_layout.addWidget(self._payloadmod_tool_bar)
        self._gain_tool_bar = Qt.QToolBar(self)
        self._gain_tool_bar.addWidget(Qt.QLabel("Gain"+": "))
        self._gain_label = Qt.QLabel(str(self.gain))
        self._gain_tool_bar.addWidget(self._gain_label)
        self.top_layout.addWidget(self._gain_tool_bar)
        self._freq_tool_bar = Qt.QToolBar(self)
        self._freq_tool_bar.addWidget(Qt.QLabel("Frequency"+": "))
        self._freq_label = Qt.QLabel(str(self.freq))
        self._freq_tool_bar.addWidget(self._freq_label)
        self.top_layout.addWidget(self._freq_tool_bar)
        self._cycleprefix_tool_bar = Qt.QToolBar(self)
        self._cycleprefix_tool_bar.addWidget(Qt.QLabel("cycleprefix"+": "))
        self._cycleprefix_line_edit = Qt.QLineEdit(str(self.cycleprefix))
        self._cycleprefix_tool_bar.addWidget(self._cycleprefix_line_edit)
        self._cycleprefix_line_edit.returnPressed.connect(
        	lambda: self.set_cycleprefix(int(self._cycleprefix_line_edit.text().toAscii())))
        self.top_layout.addWidget(self._cycleprefix_tool_bar)
        self._bandwidth_tool_bar = Qt.QToolBar(self)
        self._bandwidth_tool_bar.addWidget(Qt.QLabel("Bandwidth"+": "))
        self._bandwidth_line_edit = Qt.QLineEdit(str(self.bandwidth))
        self._bandwidth_tool_bar.addWidget(self._bandwidth_line_edit)
        self._bandwidth_line_edit.returnPressed.connect(
        	lambda: self.set_bandwidth(int(self._bandwidth_line_edit.text().toAscii())))
        self.top_layout.addWidget(self._bandwidth_tool_bar)
        self.xmlrpc_server_0 = SimpleXMLRPCServer.SimpleXMLRPCServer((ipaddress, 8080), allow_none=True)
        self.xmlrpc_server_0.register_instance(self)
        threading.Thread(target=self.xmlrpc_server_0.serve_forever).start()
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	device_addr="",
        	stream_args=uhd.stream_args(
        		cpu_format="fc32",
        		otw_format="sc8",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0.set_center_freq(freq, 0)
        self.uhd_usrp_sink_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0.set_bandwidth(bandwidth, 0)
        self.qtgui_sink_x_0 = qtgui.sink_c(
        	1024, #fftsize
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"QT GUI Plot", #name
        	True, #plotfreq
        	True, #plotwaterfall
        	True, #plottime
        	True, #plotconst
        )
        self.qtgui_sink_x_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_win = sip.wrapinstance(self.qtgui_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_sink_x_0_win)
        
        
        self._packetlen_tool_bar = Qt.QToolBar(self)
        self._packetlen_tool_bar.addWidget(Qt.QLabel("Packet Length"+": "))
        self._packetlen_label = Qt.QLabel(str(self.packetlen))
        self._packetlen_tool_bar.addWidget(self._packetlen_label)
        self.top_layout.addWidget(self._packetlen_tool_bar)
        self.fft_vxx_0 = fft.fft_vcc(cycleprefix*4, False, (()), True, 1)
        self.digital_packet_headergenerator_bb_0 = digital.packet_headergenerator_bb(header_formatter.formatter())
        self.digital_ofdm_cyclic_prefixer_0 = digital.ofdm_cyclic_prefixer(cycleprefix*4, cycleprefix*4+cycleprefix, rolloff, length_tag_key)
        self.digital_ofdm_carrier_allocator_cvc_0 = digital.ofdm_carrier_allocator_cvc(cycleprefix*4, occupied_carriers, pilot_carriers, pilot_symbols, (sync_word1, sync_word2), length_tag_key)
        self.digital_crc32_bb_0 = digital.crc32_bb(False, length_tag_key)
        self.digital_chunks_to_symbols_xx_0_0 = digital.chunks_to_symbols_bc((payloadmod.points()), 1)
        self.digital_chunks_to_symbols_xx_0 = digital.chunks_to_symbols_bc((header_mod.points()), 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate)
        self.blocks_tagged_stream_mux_0 = blocks.tagged_stream_mux(gr.sizeof_gr_complex*1, length_tag_key, 0)
        self.blocks_tag_gate_0 = blocks.tag_gate(gr.sizeof_gr_complex * 1, False)
        self.blocks_tag_debug_0 = blocks.tag_debug(gr.sizeof_char*1, "taggedstream", ""); self.blocks_tag_debug_0.set_display(True)
        self.blocks_socket_pdu_0 = blocks.socket_pdu("UDP_SERVER", ipaddress, "8082", 10000)
        self.blocks_repack_bits_bb_0 = blocks.repack_bits_bb(8, payloadmod.bits_per_symbol(), length_tag_key, False)
        self.blocks_pdu_to_tagged_stream_0 = blocks.pdu_to_tagged_stream(blocks.byte_t, "packet_len")
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((0.05, ))

        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_repack_bits_bb_0, 0), (self.digital_chunks_to_symbols_xx_0_0, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0_0, 0), (self.blocks_tagged_stream_mux_0, 1))
        self.connect((self.digital_ofdm_cyclic_prefixer_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_tag_gate_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.digital_ofdm_cyclic_prefixer_0, 0))
        self.connect((self.digital_ofdm_carrier_allocator_cvc_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.blocks_tagged_stream_mux_0, 0), (self.digital_ofdm_carrier_allocator_cvc_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.qtgui_sink_x_0, 0))
        self.connect((self.digital_crc32_bb_0, 0), (self.blocks_repack_bits_bb_0, 0))
        self.connect((self.digital_crc32_bb_0, 0), (self.digital_packet_headergenerator_bb_0, 0))
        self.connect((self.digital_chunks_to_symbols_xx_0, 0), (self.blocks_tagged_stream_mux_0, 0))
        self.connect((self.digital_packet_headergenerator_bb_0, 0), (self.digital_chunks_to_symbols_xx_0, 0))
        self.connect((self.blocks_pdu_to_tagged_stream_0, 0), (self.blocks_tag_debug_0, 0))
        self.connect((self.blocks_pdu_to_tagged_stream_0, 0), (self.digital_crc32_bb_0, 0))
        self.connect((self.blocks_tag_gate_0, 0), (self.blocks_throttle_0, 0))

        ##################################################
        # Asynch Message Connections
        ##################################################
        self.msg_connect(self.blocks_socket_pdu_0, "pdus", self.blocks_pdu_to_tagged_stream_0, "pdus")

# QT sink close method reimplementation
    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "tv13x_ofdm")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_payloadmodfrnt(self):
        return self.payloadmodfrnt

    def set_payloadmodfrnt(self, payloadmodfrnt):
        self.payloadmodfrnt = payloadmodfrnt
        self.set_payloadmod(eval(str(self.payloadmodfrnt).replace("'","")))

    def get_occupied_carriers(self):
        return self.occupied_carriers

    def set_occupied_carriers(self, occupied_carriers):
        self.occupied_carriers = occupied_carriers
        self.set_header_formatter(digital.packet_header_ofdm(self.occupied_carriers, 1, self.length_tag_key))

    def get_length_tag_key(self):
        return self.length_tag_key

    def set_length_tag_key(self, length_tag_key):
        self.length_tag_key = length_tag_key
        self.set_header_formatter(digital.packet_header_ofdm(self.occupied_carriers, 1, self.length_tag_key))

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        self.set_samp_rate(self.bandwidth)
        self._bandwidth_line_edit.setText(str(self.bandwidth))
        self.uhd_usrp_sink_0.set_bandwidth(self.bandwidth, 0)

    def get_sync_word2(self):
        return self.sync_word2

    def set_sync_word2(self, sync_word2):
        self.sync_word2 = sync_word2

    def get_sync_word1(self):
        return self.sync_word1

    def set_sync_word1(self, sync_word1):
        self.sync_word1 = sync_word1

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0.set_samp_rate(self.samp_rate)
        self.qtgui_sink_x_0.set_frequency_range(0, self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_rolloff(self):
        return self.rolloff

    def set_rolloff(self, rolloff):
        self.rolloff = rolloff

    def get_pilot_symbols(self):
        return self.pilot_symbols

    def set_pilot_symbols(self, pilot_symbols):
        self.pilot_symbols = pilot_symbols

    def get_pilot_carriers(self):
        return self.pilot_carriers

    def set_pilot_carriers(self, pilot_carriers):
        self.pilot_carriers = pilot_carriers

    def get_payloadmod(self):
        return self.payloadmod

    def set_payloadmod(self, payloadmod):
        self.payloadmod = payloadmod
        self._payloadmod_line_edit.setText(repr(self.payloadmod))

    def get_packetlen(self):
        return self.packetlen

    def set_packetlen(self, packetlen):
        self.packetlen = packetlen
        self._packetlen_label.setText(str(self.packetlen))

    def get_ipaddress(self):
        return self.ipaddress

    def set_ipaddress(self, ipaddress):
        self.ipaddress = ipaddress

    def get_header_mod(self):
        return self.header_mod

    def set_header_mod(self, header_mod):
        self.header_mod = header_mod

    def get_header_formatter(self):
        return self.header_formatter

    def set_header_formatter(self, header_formatter):
        self.header_formatter = header_formatter

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_0.set_gain(self.gain, 0)
        self._gain_label.setText(str(self.gain))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self._freq_label.setText(str(self.freq))
        self.uhd_usrp_sink_0.set_center_freq(self.freq, 0)

    def get_cycleprefix(self):
        return self.cycleprefix

    def set_cycleprefix(self, cycleprefix):
        self.cycleprefix = cycleprefix
        self._cycleprefix_line_edit.setText(str(self.cycleprefix))

if __name__ == '__main__':
    import ctypes
    import os
    if os.name == 'posix':
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    (options, args) = parser.parse_args()
    qapp = Qt.QApplication(sys.argv)
    tb = tv13x_ofdm()
    tb.start()
    tb.show()
    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()
    tb = None #to clean up Qt widgets

