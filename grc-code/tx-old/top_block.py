#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Top Block
# Generated: Fri May 20 17:58:30 2016
##################################################

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from grc_gnuradio import blks2 as grc_blks2
from optparse import OptionParser
import sip
import sys
import time

class top_block(gr.top_block, Qt.QWidget):

    def __init__(self, add="addr=192.168.10.2"):
        gr.top_block.__init__(self, "Top Block")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Top Block")
        try:
             self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
             pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Parameters
        ##################################################
        self.add = add

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 2e6
        self.packetlen = packetlen = 96
        self.ipaddress = ipaddress = "192.168.5.113"
        self.gain = gain = 20
        self.freq = freq = 2490500000
        self.cycleprefix = cycleprefix = 16
        self.bandwidth = bandwidth = 200000

        ##################################################
        # Blocks
        ##################################################
        self._packetlen_tool_bar = Qt.QToolBar(self)
        self._packetlen_tool_bar.addWidget(Qt.QLabel("Packet Length"+": "))
        self._packetlen_label = Qt.QLabel(str(self.packetlen))
        self._packetlen_tool_bar.addWidget(self._packetlen_label)
        self.top_layout.addWidget(self._packetlen_tool_bar)
        self._gain_tool_bar = Qt.QToolBar(self)
        self._gain_tool_bar.addWidget(Qt.QLabel("Gain"+": "))
        self._gain_label = Qt.QLabel(str(self.gain))
        self._gain_tool_bar.addWidget(self._gain_label)
        self.top_layout.addWidget(self._gain_tool_bar)
        self._freq_tool_bar = Qt.QToolBar(self)
        self._freq_tool_bar.addWidget(Qt.QLabel("Frequency"+": "))
        self._freq_label = Qt.QLabel(str(self.freq))
        self._freq_tool_bar.addWidget(self._freq_label)
        self.top_layout.addWidget(self._freq_tool_bar)
        self._cycleprefix_tool_bar = Qt.QToolBar(self)
        self._cycleprefix_tool_bar.addWidget(Qt.QLabel("cycleprefix"+": "))
        self._cycleprefix_line_edit = Qt.QLineEdit(str(self.cycleprefix))
        self._cycleprefix_tool_bar.addWidget(self._cycleprefix_line_edit)
        self._cycleprefix_line_edit.returnPressed.connect(
        	lambda: self.set_cycleprefix(int(self._cycleprefix_line_edit.text().toAscii())))
        self.top_layout.addWidget(self._cycleprefix_tool_bar)
        self._bandwidth_tool_bar = Qt.QToolBar(self)
        self._bandwidth_tool_bar.addWidget(Qt.QLabel("Bandwidth"+": "))
        self._bandwidth_line_edit = Qt.QLineEdit(str(self.bandwidth))
        self._bandwidth_tool_bar.addWidget(self._bandwidth_line_edit)
        self._bandwidth_line_edit.returnPressed.connect(
        	lambda: self.set_bandwidth(int(self._bandwidth_line_edit.text().toAscii())))
        self.top_layout.addWidget(self._bandwidth_tool_bar)
        self.uhd_usrp_sink_0_0 = uhd.usrp_sink(
        	device_addr="",
        	stream_args=uhd.stream_args(
        		cpu_format="fc32",
        		otw_format="sc8",
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0_0.set_samp_rate(samp_rate)
        self.uhd_usrp_sink_0_0.set_center_freq(freq, 0)
        self.uhd_usrp_sink_0_0.set_gain(gain, 0)
        self.uhd_usrp_sink_0_0.set_bandwidth(bandwidth, 0)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"QT GUI Plot", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0.set_y_axis(-140, 10)
        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_freq_sink_x_0_win)
        self.digital_ofdm_mod_0 = grc_blks2.packet_mod_b(digital.ofdm_mod(
        		options=grc_blks2.options(
        			modulation="8psk",
        			fft_length=cycleprefix*4,
        			occupied_tones=40,
        			cp_length=cycleprefix,
        			pad_for_usrp=True,
        			log=None,
        			verbose=None,
        		),
        	),
        	payload_length=packetlen,
        )
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, "/home/nodeuser/output", True)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.digital_ofdm_mod_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.digital_ofdm_mod_0, 0), (self.uhd_usrp_sink_0_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.digital_ofdm_mod_0, 0))


# QT sink close method reimplementation
    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_add(self):
        return self.add

    def set_add(self, add):
        self.add = add

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_sink_0_0.set_samp_rate(self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(0, self.samp_rate)

    def get_packetlen(self):
        return self.packetlen

    def set_packetlen(self, packetlen):
        self.packetlen = packetlen
        self._packetlen_label.setText(str(self.packetlen))

    def get_ipaddress(self):
        return self.ipaddress

    def set_ipaddress(self, ipaddress):
        self.ipaddress = ipaddress

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.uhd_usrp_sink_0_0.set_gain(self.gain, 0)
        self._gain_label.setText(str(self.gain))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self._freq_label.setText(str(self.freq))
        self.uhd_usrp_sink_0_0.set_center_freq(self.freq, 0)

    def get_cycleprefix(self):
        return self.cycleprefix

    def set_cycleprefix(self, cycleprefix):
        self.cycleprefix = cycleprefix
        self._cycleprefix_line_edit.setText(str(self.cycleprefix))

    def get_bandwidth(self):
        return self.bandwidth

    def set_bandwidth(self, bandwidth):
        self.bandwidth = bandwidth
        self._bandwidth_line_edit.setText(str(self.bandwidth))
        self.uhd_usrp_sink_0_0.set_bandwidth(self.bandwidth, 0)

if __name__ == '__main__':
    import ctypes
    import os
    if os.name == 'posix':
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"
    parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
    parser.add_option("", "--add", dest="add", type="string", default="addr=192.168.10.2",
        help="Set add [default=%default]")
    (options, args) = parser.parse_args()
    qapp = Qt.QApplication(sys.argv)
    tb = top_block(add=options.add)
    tb.start()
    tb.show()
    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()
    tb = None #to clean up Qt widgets

