import SocketServer
import os
import subprocess

class MyUDPHandler(SocketServer.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """ 
    
    
    def handle(self):
        global payloadmod_new
        
        data = self.request[0].strip()
        socket = self.request[1]
        print "{} wrote:".format(self.client_address[0])
        print data
        payloadchange = False;
        
        #update gain change
        if data.startswith('set_freq='):
            fr = int(data.replace('set_freq=',''))
            if fr!=freq_old:
                global freq_new
                freq_new=fr
                print "updated frequency changed "
                payloadchange = True;
                
        #update gain change
        if data.startswith('set_gain='):
            gn = int(data.replace('set_gain=',''))
            if gn!=gain_old:
                global gain_new
                gain_new=gn
                print "updated gain changed "
                payloadchange = True;
   
        #update bandwidth change
        if data.startswith('set_bandwidth='):
            bw = int(data.replace('set_bandwidth=',''))
            if bw!=bandwidth_old:
                global bandwidth_new
                bandwidth_new=bw
                print "updated bw changed "
                payloadchange = True;

        #update cycle prefix change
        if data.startswith('set_cycleprefix='):
            cy = int(data.replace('set_cycleprefix=',''))
            if cy!=cycle_old:
                global cycle_new
                cycle_new=cy
                payloadchange = True;
                
        #update payloadmod change
        if data.startswith('set_payloadnum='):
            
            if data.startswith('set_payloadnum=2'):
                data = 'self.payloadnum = payloadnum = 2'
                if data!=payloadmod:
                    payloadmod_new=data
                    payloadchange = True;
                    print "set self.payloadnum = payloadnum = 2"
            
            if data.startswith('set_payloadnum=4'):
                data = 'self.payloadnum = payloadnum = 4'
                if data!=payloadmod:
                    payloadmod_new=data
                    payloadchange = True;
                    print "set self.payloadnum = payloadnum = 4"
            
            if data.startswith('set_payloadnum=8'):
                data = 'self.payloadnum = payloadnum = 8'
                if data!=payloadmod:
                    payloadmod_new=data
                    payloadchange = True;
                    print "set self.payloadnum = payloadnum = 8"

            if data.startswith('set_payloadnum=16'):
                data = 'self.payloadnum = payloadnum = 16'
                if data!=payloadmod:
                    payloadmod_new=data
                    payloadchange = True;
                    print "set self.payloadnum = payloadnum = 16"

            if data.startswith('set_payloadnum=32'):
                data = 'self.payloadnum = payloadnum = 32'
                if data!=payloadmod:
                    payloadmod_new=data
                    payloadchange = True;
                    print "set self.payloadnum = payloadnum = 32"

            if data.startswith('set_payloadnum=64'):
                data = 'self.payloadnum = payloadnum = 64'
                if data!=payloadmod:
                    payloadmod_new=data
                    payloadchange = True;
                    print "set self.payloadnum = payloadnum = 64"
                    
        if payloadchange == True:
            print "call /home/nodeuser/restart.sh"
            subprocess.call("/home/nodeuser/restart.sh",shell=True)
            print "Restarting /home/nodeuser/restart.sh...."

        self.openfile()
            
        self.update_new_old()
        
    def update_new_old(self):
        global payloadmod
        global cycle_old
        global bandwidth_old
        global gain_old
        global freq_old
        
        payloadmod=payloadmod_new
        cycle_old=cycle_new
        bandwidth_old=bandwidth_new
        gain_old=gain_new
        freq_old=freq_new
        
        

    def transform_line(self, line):
        wordReplacements = {payloadmod:payloadmod_new,
                            cycleprefix:cycleprefix+str(cycle_new),
                            bandwidth:bandwidth+str(bandwidth_new),
                            gain:gain+str(gain_new),
                            freq:freq+str(freq_new)}
        for key, value in wordReplacements.iteritems():
            if key in line:
                # You need to include a newline if you're replacing the whole line
                line = "        "+value+"\n"
                print line
        #    line = line.replace(key, value)
        #    print key
                
        return line

    def openfile(self):
        with open("output.txt", "w") as output_file, open("txrx26x_ofdm.py") as input_file:
            for line in input_file:
                output_file.write(self.transform_line(line))

        os.rename("output.txt", "txrx26x_ofdm.py")


if __name__ == "__main__":
    HOST, PORT = "192.168.5.111", 8083
    
    freq_new = 2490500000
    freq_old = 2490500000;
    freq = 'self.freq = freq = '
        
    gain_new = 20
    gain_old = 20
    gain = 'self.gain = gain = '
        
    bandwidth_new = 200000
    bandwidth_old = 200000;
    bandwidth = 'self.bandwidth = bandwidth = '
        
    cycle_new = 16
    cycle_old = 16;
    cycleprefix = 'self.cycleprefix = cycleprefix = '
        
    payloadmod_new = 'self.payloadnum = payloadnum = 8'
    payloadmod = 'self.payloadnum = payloadnum = '
    
    # Create the server, binding to localhost on port 9999
    try:
        server = SocketServer.UDPServer((HOST, PORT), MyUDPHandler)
    except OSError as e:
        print("Could not listen on port {}".format(port))

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
    

