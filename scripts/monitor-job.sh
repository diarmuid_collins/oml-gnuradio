#!/bin/bash

source /etc/profile
source /etc/bash.bashrc
source /etc/environment
env


c=1
while [[ $c -le 5 ]]; do

	processgr=`ps -ef | grep -c "grserver"`
	
	if [ "${processgr}" -le "1" ] ; then
	#        cp /home/nodeuser/tv26x_ofdm_original.py /home/nodeuser/tv26x_ofdm.py
	        chmod +x /home/nodeuser/txrx26x_ofdm.py
	        nohup python -u /home/nodeuser/grserver.py >> /home/nodeuser/grserver.log &
	fi
	
	
	processtv14x=`ps -ef | grep -c "txrx26x_ofdm"`
	
	if [ "${processtv14x}" -le "1" ] ; then
	    chmod +x /home/nodeuser/restart.sh
		nohup /home/nodeuser/restart.sh >> /home/nodeuser/restart.txt &
	fi
	
	exception=`/bin/grep -c "Exception happened during processing of request from"  /home/nodeuser/restart.txt`
	echo $exception
	if [ "${exception}" -ge "1" ] ; then
		chmod +x /home/nodeuser/restart.sh
		rm /home/nodeuser/restart.txt
		nohup /home/nodeuser/restart.sh > /home/nodeuser/restart.txt &
	fi
	
	exception=`/bin/grep -c "Exception happened during processing of request from"  /home/nodeuser/grserver.log`
	echo $exception
	if [ "${exception}" -ge "1" ] ; then
		chmod +x /home/nodeuser/restart.sh
		rm /home/nodeuser/restart.txt
		nohup /home/nodeuser/restart.sh > /home/nodeuser/restart.txt &
	fi
	
	sleep 0.2
done
