#!/bin/bash

source /etc/profile
source /etc/bash.bashrc
source /etc/environment
env

/bin/kill -9 `ps -ef | grep "txrx" | awk '{ print $2 }'`

/bin/rm /home/nodeuser/nohup.out
/bin/chmod +x /home/nodeuser/txrx26x_ofdm.py
/usr/bin/nohup /usr/bin/python /home/nodeuser/txrx26x_ofdm.py &
