#!/bin/bash

#find the USRP IP address
usrpip=`/usr/bin/uhd_find_devices | grep addr | sed 's/    addr: //g'`

/bin/sed -i -e "s/192.168.10.3/$usrpip/g" /home/nodeuser/oml-gnuradio/grc-code/rv26x_ofdm.py 

/bin/sed -i -e "s/192.168.10.3/$usrpip/g" /home/nodeuser/oml-gnuradio/grc-code/tv26x_ofdm.py

