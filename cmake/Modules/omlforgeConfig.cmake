INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_OMLFORGE omlforge)

FIND_PATH(
    OMLFORGE_INCLUDE_DIRS
    NAMES omlforge/api.h
    HINTS $ENV{OMLFORGE_DIR}/include
        ${PC_OMLFORGE_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    OMLFORGE_LIBRARIES
    NAMES gnuradio-omlforge
    HINTS $ENV{OMLFORGE_DIR}/lib
        ${PC_OMLFORGE_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(OMLFORGE DEFAULT_MSG OMLFORGE_LIBRARIES OMLFORGE_INCLUDE_DIRS)
MARK_AS_ADVANCED(OMLFORGE_LIBRARIES OMLFORGE_INCLUDE_DIRS)

