# CMake generated Testfile for 
# Source directory: /home/nodeuser/oml-gnuradio
# Build directory: /home/nodeuser/oml-gnuradio/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(include/omlforge)
SUBDIRS(lib)
SUBDIRS(swig)
SUBDIRS(python)
SUBDIRS(grc)
SUBDIRS(apps)
SUBDIRS(docs)
