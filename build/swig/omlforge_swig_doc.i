
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") oml_inject_frequencyplotmp "

Params: (mp, xdata, ydata)"

%feature("docstring") oml_inject_timeplotmp "

Params: (mp, realdata, imgdata, ydata)"

%feature("docstring") oml_inject_constplotmp "

Params: (mp, xdata, ydata)"

%feature("docstring") oml_inject_waterfallplotmp "

Params: (mp, ydata, xdata, droppedFrames)"

%feature("docstring") gr::omlforge::c_const_sink "<+description of block+>"

%feature("docstring") gr::omlforge::c_const_sink::make "Return a shared_ptr to a new instance of omlforge::c_const_sink.

To avoid accidental use of raw pointers, omlforge::c_const_sink's constructor is in a private implementation class. omlforge::c_const_sink::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::omlforge::c_freq_sink "<+description of block+>"

%feature("docstring") gr::omlforge::c_freq_sink::make "Return a shared_ptr to a new instance of omlforge::c_freq_sink.

To avoid accidental use of raw pointers, omlforge::c_freq_sink's constructor is in a private implementation class. omlforge::c_freq_sink::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::omlforge::c_sink "<+description of block+>"

%feature("docstring") gr::omlforge::c_sink::make "Return a shared_ptr to a new instance of omlforge::c_sink.

To avoid accidental use of raw pointers, omlforge::c_sink's constructor is in a private implementation class. omlforge::c_sink::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::omlforge::c_time_sink "<+description of block+>"

%feature("docstring") gr::omlforge::c_time_sink::make "Return a shared_ptr to a new instance of omlforge::c_time_sink.

To avoid accidental use of raw pointers, omlforge::c_time_sink's constructor is in a private implementation class. omlforge::c_time_sink::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::omlforge::c_waterfall_null "<+description of block+>"

%feature("docstring") gr::omlforge::c_waterfall_null::make "Return a shared_ptr to a new instance of omlforge::c_waterfall_null.

To avoid accidental use of raw pointers, omlforge::c_waterfall_null's constructor is in a private implementation class. omlforge::c_waterfall_null::make is the public interface for creating new instances.

Params: (NONE)"

%feature("docstring") gr::omlforge::c_waterfall_sink "<+description of block+>"

%feature("docstring") gr::omlforge::c_waterfall_sink::make "Return a shared_ptr to a new instance of omlforge::c_waterfall_sink.

To avoid accidental use of raw pointers, omlforge::c_waterfall_sink's constructor is in a private implementation class. omlforge::c_waterfall_sink::make is the public interface for creating new instances.

Params: (NONE)"