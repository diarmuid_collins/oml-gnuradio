var classgr_1_1omlforge_1_1c__waterfall__sink__impl =
[
    [ "c_waterfall_sink_impl", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#ade476c17337a0b8eb24b9a95c67af097", null ],
    [ "~c_waterfall_sink_impl", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#a5bdd9abe380b48ac29d1522cc14dbdab", null ],
    [ "fft_average", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#a4d6f4320e96296db9b653657017f519e", null ],
    [ "fft_size", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#a7a7b664504545492307a51963dd65016", null ],
    [ "fftresize", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#a86fac5d25fda9f6ef889e70e8d23e215", null ],
    [ "set_fft_average", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#a72dc3d230d4e351564ed5ec698780594", null ],
    [ "set_fft_size", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#ae96298ac786b3d5349ce9ea38e140cd9", null ],
    [ "set_frequency_range", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#a8ad79231e5769f73a07f595542150d4d", null ],
    [ "set_update_time", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#af79a5b9ca12d7f79089c6763aca86a5b", null ],
    [ "work", "classgr_1_1omlforge_1_1c__waterfall__sink__impl.html#ad66b0b3b5276c76337955b366b978272", null ]
];