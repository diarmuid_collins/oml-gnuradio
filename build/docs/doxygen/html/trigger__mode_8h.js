var trigger__mode_8h =
[
    [ "trigger_mode", "trigger__mode_8h.html#ac3620da12b64e2da7164baea5e30d3f7", [
      [ "TRIG_MODE_FREE", "trigger__mode_8h.html#ac3620da12b64e2da7164baea5e30d3f7a2056c249fbb00fa78f4f7ea9acd4ef3c", null ],
      [ "TRIG_MODE_AUTO", "trigger__mode_8h.html#ac3620da12b64e2da7164baea5e30d3f7a50cd96954356d2c12141fa6af23521fe", null ],
      [ "TRIG_MODE_NORM", "trigger__mode_8h.html#ac3620da12b64e2da7164baea5e30d3f7ab1a4414da57898158d515a054a47ff0b", null ],
      [ "TRIG_MODE_TAG", "trigger__mode_8h.html#ac3620da12b64e2da7164baea5e30d3f7a151de522ed36b0db3c2390ccc9ff887c", null ]
    ] ],
    [ "trigger_slope", "trigger__mode_8h.html#a823308745ee3c2a77b302453451cde98", [
      [ "TRIG_SLOPE_POS", "trigger__mode_8h.html#a823308745ee3c2a77b302453451cde98ab3778ca19f046b8b2ce710ea11582d3b", null ],
      [ "TRIG_SLOPE_NEG", "trigger__mode_8h.html#a823308745ee3c2a77b302453451cde98a57c610cdd524fd9156ef5a2a790497ea", null ]
    ] ]
];