/* -*- c++ -*- */

#define OMLFORGE_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "omlforge_swig_doc.i"

%{
#include "omlforge/c_const_sink.h"
#include "omlforge/c_sink.h"
#include "omlforge/c_freq_sink.h"
#include "omlforge/c_waterfall_sink.h"
#include "omlforge/c_time_sink.h"
#include "omlforge/c_waterfall_null.h"
%}

%include "omlforge/c_const_sink.h"
GR_SWIG_BLOCK_MAGIC2(omlforge, c_const_sink);
%include "omlforge/c_sink.h"
GR_SWIG_BLOCK_MAGIC2(omlforge, c_sink);
%include "omlforge/c_freq_sink.h"
GR_SWIG_BLOCK_MAGIC2(omlforge, c_freq_sink);
%include "omlforge/c_waterfall_sink.h"
GR_SWIG_BLOCK_MAGIC2(omlforge, c_waterfall_sink);
%include "omlforge/c_time_sink.h"
GR_SWIG_BLOCK_MAGIC2(omlforge, c_time_sink);
%include "omlforge/c_waterfall_null.h"
GR_SWIG_BLOCK_MAGIC2(omlforge, c_waterfall_null);
